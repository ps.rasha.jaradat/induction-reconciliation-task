package com.progressoft.generator;

import com.progressoft.reconciliation.ResponseGenerator;

public class GeneratorFactory {
    private GeneratorFactory() {
    }

    public static ResponseGenerator getGenerator(String type , String path) {
        if ("csv".equals(type.toLowerCase()))
            return new CsvGenerator(path);
        else return new JsonGenerator(path);
    }
}
