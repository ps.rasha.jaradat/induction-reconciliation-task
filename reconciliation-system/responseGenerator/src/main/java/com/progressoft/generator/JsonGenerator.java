package com.progressoft.generator;

import com.google.gson.*;
import com.progressoft.entities.JsonRecord;
import com.progressoft.reconciliation.ReconCompareResult;
import com.progressoft.reconciliation.ResponseGenerator;
import com.progressoft.reconciliation.TransactionRecord;
import com.progressoft.reconciliation.TransactionTypePair;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class JsonGenerator extends ResponseGenerator<String> {
    private Path matchingPath;
    private Path missMatchingPath;
    private Path missingPath;

    public JsonGenerator(String path) {
        super(path);
    }

    @Override
    protected void writeResultOnFiles(ReconCompareResult result, String path) {
        createResultDirectoryWith3Files(path);
        writeMatchTran(result.getMatching(), matchingPath);
        writeNotMachTran(result.getMissMatching(), missMatchingPath);
        writeNotMachTran(result.getMissing(), missingPath);
    }

    @Override
    protected String getMissingTransactions() {
        return missingPath.toString();
    }

    @Override
    protected String getMismatchingTransactions() {
        return missMatchingPath.toString();
    }

    @Override
    protected String getMatchingTransactions() {
        return matchingPath.toString();
    }

    private void createResultDirectoryWith3Files(String path) {
        try {
            Path resultDirectoryPath = Files.createDirectory(Paths.get(path, "/recon_result"));
            matchingPath = Files.createFile(Paths.get(resultDirectoryPath + "/matching.json"));
            missMatchingPath = Files.createFile(Paths.get(resultDirectoryPath + "/miss_matching.json"));
            missingPath = Files.createFile(Paths.get(resultDirectoryPath + "/missing.json"));
        } catch (FileAlreadyExistsException e) {
            throw new RuntimeException("There is already a file named recon_result in " + System.getProperty("user.home"));
        } catch (IOException e) {
            throw new IllegalStateException("Error while generating result files!", e);
        }
    }

    public void writeNotMachTran(List<TransactionTypePair> notMatching, Path notMatchingPath) {
        Gson gson = new Gson();
        try (PrintWriter wr = new PrintWriter(Files.newOutputStream(notMatchingPath))) {
            List<NotMatchingRecord> notMatchingRecords = new ArrayList<>();
            for (TransactionTypePair r : notMatching) {
                notMatchingRecords.add(new NotMatchingRecord(r));
            }
            String s = gson.toJson(notMatchingRecords);
            wr.println(s);
        } catch (IOException e) {
            throw new RuntimeException("Path Not found !");
        }
    }

    public void writeMatchTran(List<? extends TransactionRecord> matching, Path matchingPath) {
        Gson gson = new Gson();
        try (PrintWriter wr = new PrintWriter(Files.newOutputStream(matchingPath))) {
            List<MatchingRecord> matchingRecords = new ArrayList<>();
            for (TransactionRecord r : matching) {
                matchingRecords.add(new MatchingRecord(r));
            }
            String s = gson.toJson(matchingRecords);
            wr.println(s);
        } catch (IOException e) {
            throw new RuntimeException("Path Not found !");

        }
    }

}

class MatchingRecord {
    private String date;
    private String reference;
    private BigDecimal amount;
    private String currencyCode;

    MatchingRecord(TransactionRecord jsonRecord) {
        this.date = jsonRecord.getTransactionDate().format(DateTimeFormatter.ofPattern("dd/MM/YYYY"));
        this.reference = jsonRecord.getTransactionId();
        this.amount = jsonRecord.getTransactionAmount();
        this.currencyCode = jsonRecord.getTransactionCurrencyCode();
    }

}

class NotMatchingRecord {
    private String from;
    private String date;
    private String reference;
    private BigDecimal amount;
    private String currencyCode;

    NotMatchingRecord(TransactionTypePair transactionTypePair) {
        this.date = transactionTypePair.getRecord().getTransactionDate().format(DateTimeFormatter.ofPattern("dd/MM/YYYY"));
        this.reference = transactionTypePair.getRecord().getTransactionId();
        this.amount = transactionTypePair.getRecord().getTransactionAmount();
        this.currencyCode = transactionTypePair.getRecord().getTransactionCurrencyCode();
        this.from = transactionTypePair.getType();
    }

}
