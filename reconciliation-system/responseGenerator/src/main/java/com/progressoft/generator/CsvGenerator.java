package com.progressoft.generator;

import com.progressoft.reconciliation.ReconCompareResult;
import com.progressoft.reconciliation.ResponseGenerator;
import com.progressoft.reconciliation.TransactionRecord;
import com.progressoft.reconciliation.TransactionTypePair;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class CsvGenerator extends ResponseGenerator<String> {
    private Path matchingPath;
    private Path missMatchingPath;
    private Path missingPath;

    public CsvGenerator(String path) {
        super(path);
    }

    @Override
    protected void writeResultOnFiles(ReconCompareResult result, String path) {
        createResultDirectoryWith3Files(path);
        String missingHeader = "found in file,transaction id,amount,currency code,value date";
        String missMatchingHeader = "found in file,transaction id,amount,currecny code,value date";
        writeMatchTran(result.getMatching(), matchingPath);
        writeNotMachTran(result.getMissMatching(), missMatchingPath, missMatchingHeader);
        writeNotMachTran(result.getMissing(), missingPath, missingHeader);
    }

    @Override
    protected String getMissingTransactions() {
        return missingPath.toString();
    }

    @Override
    protected String getMismatchingTransactions() {
        return missMatchingPath.toString();
    }

    @Override
    protected String getMatchingTransactions() {
        return matchingPath.toString();
    }

    private void createResultDirectoryWith3Files(String path) {
        try {
            Path resultDirectoryPath = Files.createDirectory(Paths.get(path, "/recon_result"));
            matchingPath = Files.createFile(Paths.get(resultDirectoryPath + "/matching.csv"));
            missMatchingPath = Files.createFile(Paths.get(resultDirectoryPath + "/miss_matching.csv"));
            missingPath = Files.createFile(Paths.get(resultDirectoryPath + "/missing.csv"));
        } catch (FileAlreadyExistsException e) {
            throw new RuntimeException("There is already a file named recon_result in " + System.getProperty("user.home"));
        } catch (IOException e) {
            throw new IllegalStateException("Error while generating result files!", e);
        }
    }

    private void writeNotMachTran(List<TransactionTypePair> list, Path path, String header) {
        try (FileWriter fw = new FileWriter(path.toString(), true);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter out = new PrintWriter(bw)) {
            out.println(header);
            for (int i = 0; i < list.size(); i++) {
                writeOnNotMatchFile(list.get(i), out);
            }
        } catch (IOException e) {
            throw new IllegalStateException("Error while generating result files!", e);
        }
    }

    private void writeOnNotMatchFile(TransactionTypePair record, PrintWriter out) {
        out.println(record.getType() + "," + record.getRecord().getTransactionId() +
                "," + record.getRecord().getTransactionAmount() +
                "," + record.getRecord().getTransactionCurrencyCode() +
                "," + record.getRecord().getTransactionDate().format(DateTimeFormatter.ISO_LOCAL_DATE));
    }

    private void writeMatchTran(List<TransactionRecord> list, Path path) {
        String matchingHeader = "transaction id,amount,currecny code,value date";
        try (FileWriter fw = new FileWriter(path.toString(), true);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter out = new PrintWriter(bw)) {
            out.println(matchingHeader);
            for (int i = 0; i < list.size(); i++) {
                writeOnMatchFile(list.get(i), out);
            }
        } catch (IOException e) {
            throw new IllegalStateException("Error occurred while opening or creating result files!", e);
        }
    }

    private void writeOnMatchFile(TransactionRecord tran, PrintWriter out) {
        out.println(tran.getTransactionId() + ","
                + tran.getTransactionAmount() + ","
                + tran.getTransactionCurrencyCode()
                + "," + tran.getTransactionDate().format(DateTimeFormatter.ISO_LOCAL_DATE));
    }
}
