package com.progressoft.generator;
import com.progressoft.reconciliation.ReconCompareResult;
import com.progressoft.reconciliation.TransactionRecord;
import com.progressoft.reconciliation.TransactionTypePair;
import org.junit.jupiter.api.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Disabled
public class JsonGeneratorTest {
    private final String RESOURCE_PATH = System.getProperty("user.home");
    private List<TransactionRecord> matching;
    private List<TransactionTypePair> missing;
    private List<TransactionTypePair> missMatching;

    @Test
    public void givenNullReconResult_whenCreatingFileResultGenerator_thenThrowNullPointerException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> new JsonGenerator(RESOURCE_PATH).generate(null));
        Assertions.assertEquals("Given Null result !", exception.getMessage());
    }

    @Test
    public void givenReconResultWithNullLists__whenCreatingFileResultGenerator_thenThrowNullPointerException() {
        matching = new ArrayList<>();
        missing = new ArrayList<>();
        missMatching = new ArrayList<>();
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> new JsonGenerator(RESOURCE_PATH).generate(new MockReconCompareResult(null, missing, missMatching)));
        Assertions.assertEquals("Something went wrong, matching transaction are null!", exception.getMessage());
        exception = Assertions.assertThrows(NullPointerException.class,
                () -> new JsonGenerator(RESOURCE_PATH).generate(new MockReconCompareResult(matching, null, missMatching)));
        Assertions.assertEquals("Something went wrong, missing transaction are null!", exception.getMessage());
        exception = Assertions.assertThrows(NullPointerException.class,
                () -> new JsonGenerator(RESOURCE_PATH).generate(new MockReconCompareResult(matching, missing, null)));
        Assertions.assertEquals("Something went wrong, missMatching transaction are null!", exception.getMessage());
    }

    @Test
    public void whenGenerate_whileThereIsAlreadyResultFile_thenThrowRuntimeException() throws IOException {
        List<TransactionRecord> matchingList = new ArrayList<>();
        List<TransactionTypePair> missingList = new ArrayList<>();
        List<TransactionTypePair> missMatchingList = new ArrayList<>();
        Files.createDirectory(Paths.get(RESOURCE_PATH, "recon_result"));
        RuntimeException exception = Assertions.assertThrows(RuntimeException.class,
                () -> new JsonGenerator(RESOURCE_PATH).writeResultOnFiles(new ReconCompareResult(matchingList, missingList, missMatchingList) , RESOURCE_PATH));
        Assertions.assertEquals("There is already a file named recon_result in /home/user", exception.getMessage());
        Files.delete(Paths.get(RESOURCE_PATH + "/recon_result"));
    }

    @Nested
    class testingCreationOfFiles {
        private Path matchingPath = Paths.get(RESOURCE_PATH + "/recon_result/matching.json");
        private Path missMatchingPath = Paths.get(RESOURCE_PATH + "/recon_result/miss_matching.json");
        private Path missingPath = Paths.get(RESOURCE_PATH + "/recon_result/missing.json");
        private List<String> actual;

       @BeforeEach
        public void beforeEach() {
            List<TransactionRecord> matchingList = new ArrayList<>();
            List<TransactionTypePair> missingList = new ArrayList<>();
            List<TransactionTypePair> missMatchingList = new ArrayList<>();
            JsonGenerator fileResultGenerator = new JsonGenerator(RESOURCE_PATH);
            actual = fileResultGenerator.generate(new ReconCompareResult(matchingList, missingList, missMatchingList));
        }

        @AfterEach
        public void deleteCreatedFiles() throws IOException {
            Files.delete(missMatchingPath);
            Files.delete(matchingPath);
            Files.delete(missingPath);
            Files.delete(Paths.get(RESOURCE_PATH + "/recon_result"));
        }

        @Test
        public void whenGenerate_thenDirectoryWith3FilesBuiltSuccessfully() {
            Assertions.assertTrue(Files.exists(Paths.get(RESOURCE_PATH, "recon_result")));
            Assertions.assertTrue(Files.exists(missMatchingPath));
            Assertions.assertTrue(Files.exists(matchingPath));
            Assertions.assertTrue(Files.exists(missingPath));
        }

        @Test
        public void whenGenerate_thenReturnListOfFilesPath() {
            List<String> expected = new ArrayList<>();
            expected.add(matchingPath.toString());
            expected.add(missMatchingPath.toString());
            expected.add(missingPath.toString());
            for (int i = 0; i < actual.size(); i++) {
                Assertions.assertEquals(expected.get(i), actual.get(i));
            }
        }

    }
}

