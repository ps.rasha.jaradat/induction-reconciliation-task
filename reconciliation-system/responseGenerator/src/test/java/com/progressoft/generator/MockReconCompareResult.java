package com.progressoft.generator;

import com.progressoft.reconciliation.ReconCompareResult;
import com.progressoft.reconciliation.TransactionRecord;
import com.progressoft.reconciliation.TransactionTypePair;

import java.util.List;


public class MockReconCompareResult extends ReconCompareResult {
    private List<TransactionRecord> matching;
    private List<TransactionTypePair> missing;
    private List<TransactionTypePair> missMatching;
    public MockReconCompareResult(List<TransactionRecord> matching, List<TransactionTypePair> missing, List<TransactionTypePair> missMatching) {
        super(matching, missing, missMatching);
        this.matching = matching;
        this.missing = missing;
        this.missMatching = missMatching;
    }

    @Override
    public List<TransactionRecord> getMatching() {
        return matching;
    }

    @Override
    public List<TransactionTypePair> getMissMatching() {
        return missMatching;
    }

    @Override
    public List<TransactionTypePair> getMissing() {
        return missing;
    }
}
