package com.progressoft.entities;
import java.math.BigDecimal;
public class NotMatchingRecord{
    private String from;
    private String date;
    private String reference;
    private BigDecimal amount;
    private String currencyCode;
    public NotMatchingRecord(String from, String date, String reference, BigDecimal amount, String currencyCode) {
        this.from = from;
        this.date = date;
        this.reference = reference;
        this.amount = amount;
        this.currencyCode = currencyCode;
    }
    public String getFrom() {
        return this.from;
    }
    public String getDate() {
        return this.date;
    }
    public String getReference() {
        return this.reference;
    }
    public BigDecimal getAmount() {
        return this.amount;
    }
    public String getCurrencyCode() {
        return this.currencyCode;
    }
}
