package com.progressoft.entities;
import java.math.BigDecimal;
public class MatchingRecord{
    private String date;
    private String reference;
    private BigDecimal amount;
    private String currencyCode;
    public MatchingRecord(String date, String reference, BigDecimal amount, String currencyCode) {
        this.date = date;
        this.reference = reference;
        this.amount = amount;
        this.currencyCode = currencyCode;
    }
    public String getFrom() {
        return "";
    }
    public String getDate() {
        return this.date;
    }
    public String getReference() {
        return this.reference;
    }
    public BigDecimal getAmount() {
        return this.amount;
    }
    public String getCurrencyCode() {
        return this.currencyCode;
    }
}
