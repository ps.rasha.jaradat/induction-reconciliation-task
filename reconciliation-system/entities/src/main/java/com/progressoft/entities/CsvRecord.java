package com.progressoft.entities;

import com.progressoft.reconciliation.TransactionRecord;

import java.math.BigDecimal;
import java.time.LocalDate;

public class CsvRecord extends TransactionRecord {
    private String id;
    private BigDecimal amount;
    private String currency;
    private LocalDate date;

    public CsvRecord(String id, BigDecimal amount, String currency, LocalDate date) {
        this.id = id;
        this.amount = amount;
        this.currency = currency;
        this.date = date;
    }

    @Override
    public String getTransactionId() {
        return id;
    }

    @Override
    public BigDecimal getTransactionAmount() {
        return amount;
    }

    @Override
    public LocalDate getTransactionDate() {
        return date;
    }

    @Override
    public String getTransactionCurrencyCode() {
        return currency;
    }
}
