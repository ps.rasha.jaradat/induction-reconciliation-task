package com.progressoft.entities;

import com.progressoft.reconciliation.TransactionRecord;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class JsonRecord extends TransactionRecord {
    private LocalDate date;
    private String reference;
    private BigDecimal amount;
    private String currencyCode;
    private String purpose;

    public JsonRecord(LocalDate date, String reference, BigDecimal amount, String currencyCode, String purpose) {
        this.date = date;
        this.reference = reference;
        this.amount = amount;
        this.currencyCode = currencyCode;
        this.purpose = purpose;
    }

    @Override
    public String getTransactionId() {
        return reference;
    }

    @Override
    public BigDecimal getTransactionAmount() {
        return amount;
    }

    @Override
    public LocalDate getTransactionDate() {
        return date;
    }

    @Override
    public String getTransactionCurrencyCode() {
        return currencyCode;
    }

}
