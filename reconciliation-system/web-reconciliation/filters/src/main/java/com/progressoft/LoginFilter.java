package com.progressoft;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

public class LoginFilter implements Filter {
    private Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @Override
    public void init(FilterConfig filterConfig) {
        logger.info("Login filter initialized");
    }
    @Override
    public void destroy() {
        logger.info("Login filter destroyed");
    }
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        logger.info("Check if user already logged in");

        String userId = (String) req.getSession().getAttribute("userId");
        String curSession = req.getSession(false).getId();

        if (!(curSession.equals(userId))) {
            logger.info("User did not logged in");
            resp.sendRedirect(req.getContextPath());
        }
        else{
            logger.info("User already logged in");
            chain.doFilter(req, resp);
        }
    }
}

