package com.progressoft;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;
import java.util.logging.Logger;

public class RenderingResultFilter implements Filter {
    private Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info("Rendering filter initialized");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        logger.info("check if results are available to render");
        if (Objects.isNull(req.getSession().getAttribute("resultDir"))) {
            logger.info("Results are not available to render");
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "No Result to show !");
        } else {
            logger.info("Results are available to render");
            chain.doFilter(req, resp);
        }
    }

    @Override
    public void destroy() {
        logger.info("Rendering filter destroyed");
    }
}
