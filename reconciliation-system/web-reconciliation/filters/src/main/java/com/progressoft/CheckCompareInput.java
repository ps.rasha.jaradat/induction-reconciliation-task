package com.progressoft;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Logger;

public class CheckCompareInput implements Filter {
    // TODO those are wrong ..Done

    private Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @Override
    public void init(FilterConfig filterConfig) {
        logger.info("Compare filter initialized");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        // TODO not clean behavior, you should check the parameters and parts without depending on ServletException..Done
        // TODO it seems this attribute is not cleaned ???!!!..Done

        HttpServletRequest req;
        HttpServletResponse resp;
        req = (HttpServletRequest) request;
        resp = (HttpServletResponse) response;
        logger.info("Check inputs to compare");
        if (isRequestWithWrongParam(req)) {
            logger.info("Compare parameters are Null");
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "No result to compare !!");
        }
        logger.info("Compare parameters available");
        chain.doFilter(req, resp);
    }

    @Override
    public void destroy() {
        logger.info("Compare filter destroyed");
    }

    private boolean validateParameters(HttpServletRequest req) throws ServletException, IOException {
        return isPartNull(req.getPart("sourceFile")) &&
                isPartNull(req.getPart("targetFile")) &&
                isParamNull(req.getParameter("targetFileName")) &&
                isParamNull(req.getParameter("targetFileType")) &&
                isParamNull(req.getParameter("sourceFileName")) &&
                isParamNull(req.getParameter("sourceFileType"));
    }

    private boolean isPartNull(Part file) {
        if (Objects.isNull(file))
            return true;
        return false;
    }

    private boolean isParamNull(String param) {
        if (Objects.isNull(param))
            return true;
        return false;
    }

    private boolean isRequestWithWrongParam(HttpServletRequest req) {
        Map<String, String[]> parameterMap = req.getParameterMap();
        if (parameterMap.isEmpty() || parameterMap.size() != 6)
            return true;
        return false;
    }
}
