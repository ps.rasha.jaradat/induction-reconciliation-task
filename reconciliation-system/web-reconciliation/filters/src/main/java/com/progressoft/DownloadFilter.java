package com.progressoft;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;
import java.util.logging.Logger;

public class DownloadFilter implements Filter {
    // TODO same note ..Done

    private Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @Override
    public void init(FilterConfig filterConfig) {
        logger.info("Downloading filter initialized");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req;
        HttpServletResponse resp;
        req = (HttpServletRequest) request;
        resp = (HttpServletResponse) response;
        String path = (String) req.getSession().getAttribute("resultDir");
        if (Objects.isNull(path)) {
            logger.info("Result directory path is not available to download");
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "No files to download");
        } else {
            logger.info("Result directory path is available to download");
            chain.doFilter(req, resp);
        }
    }

    @Override
    public void destroy() {
    }
}
