package com.progressoft;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

public class SetUserInfoFilter implements Filter {
    private Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @Override
    public void init(FilterConfig filterConfig) {
        logger.info("Set user info filter initialized");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        if (!req.getMethod().equalsIgnoreCase("POST")) {
            chain.doFilter(req, resp);
            return;
        }
        logger.info("Check user name and password ");
        String userName = req.getParameter("userName");
        String pwd = req.getParameter("pwd");
        if (!isValidInfo(userName, pwd)) {
            logger.info("Not valid user name or password");
            resp.sendRedirect(req.getContextPath());
            return;
        }
        String id = req.getSession(false).getId();
        req.getSession().setAttribute("userId", id);
        chain.doFilter(req, resp);
    }

    @Override
    public void destroy() {
        logger.info("Set user info filter destroyed");
    }

    private boolean isValidInfo(String username, String password) {
        String USERNAME = "user";
        String PASSWORD = "pass";
        return (USERNAME.equals(username) && PASSWORD.equals(password));
    }
}


