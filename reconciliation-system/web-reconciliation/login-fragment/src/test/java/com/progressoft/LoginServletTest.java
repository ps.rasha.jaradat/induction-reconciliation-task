package com.progressoft;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.PrintWriter;
import java.io.StringWriter;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LoginServletTest {
    @Test
    public void whenDoGet_thenForwardToLoginPage() {
        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        RequestDispatcher requestDispatcher = Mockito.mock(RequestDispatcher.class);
        HttpSession session = Mockito.mock(HttpSession.class);
        try {
            when(response.getWriter()).thenReturn(new PrintWriter(new StringWriter()));
            when(request.getSession()).thenReturn(session);
            when(request.getRequestDispatcher("/WEB-INF/login.jsp")).thenReturn(requestDispatcher);
            new LoginServlet().doGet(request, response);
            verify(requestDispatcher).forward(request, response);
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }
}

