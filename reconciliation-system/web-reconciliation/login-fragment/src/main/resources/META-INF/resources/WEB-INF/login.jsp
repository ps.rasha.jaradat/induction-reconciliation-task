<%@taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Reconciliation System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
        #loginInfo {
            background-color: #ffffff;
            margin: 100px auto;
            padding: 40px;
            width: 70%;
            min-width: 300px;
        }
    </style>
</head>

<body>
<div id="loginInfo" class="panel panel-default container">
    <h2>Login Info </h2>
    <form method="POST" action="${pageContext.request.contextPath}/input">
        <div class="form-group ">
            <label for="user-name">User Name:</label>
            <input type="text" class="form-control" id="user-name" placeholder="Enter user name" name="userName" required>
        </div>
        <div class="form-group">
            <label for="pwd">Password:</label>
            <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd" required>
        </div>
        <button type="submit" class="btn btn-primary" style="float:right;" >Login</button>
    </form>
</div>
</body>
</html>
