package com.progressoft;

import com.progressoft.reconcileUseCase.ReconUseCase;
import com.progressoft.validator.RequestValidator;
import com.progressoft.validator.Validator;

import javax.servlet.*;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Set;

public class Initializer implements ServletContainerInitializer {
    @Override
    public void onStartup(Set<Class<?>> c, ServletContext ctx) {
        try {
            registerInputServlet(ctx);
            registerLoginServlet(ctx);
            registerDownloadServlet(ctx);
            registerRenderingServlet(ctx);
            registerCompareServlet(ctx);
            registerSetUserInfoFilter(ctx);
            registerLoginFilter(ctx);
           // registerCheckCompareFilter(ctx);
            registerDownloadFilter(ctx);
            registerRenderFilter(ctx);
        } catch (IOException e) {
            throw new RuntimeException("Error while configuring upload servlet !");
        }
    }

    private void registerLoginFilter(ServletContext ctx) {
        LoginFilter loginFilter = new LoginFilter();
        FilterRegistration.Dynamic registration = ctx.addFilter("login-filter", loginFilter);
        registration.addMappingForServletNames(null, true,
                "input", "compare", "download", "render");
    }

    private void registerDownloadFilter(ServletContext ctx) {
        DownloadFilter downloadFilter = new DownloadFilter();
        FilterRegistration.Dynamic registration = ctx.addFilter("download-filter", downloadFilter);
        registration.addMappingForServletNames(null, true, "download");
    }

    private void registerRenderFilter(ServletContext ctx) {
        RenderingResultFilter renderingResultFilter = new RenderingResultFilter();
        FilterRegistration.Dynamic registration = ctx.addFilter("render-filter", renderingResultFilter);
        registration.addMappingForServletNames(null, true, "render");
    }

    private void registerCheckCompareFilter(ServletContext ctx) {
        CheckCompareInput checkCompareFilter = new CheckCompareInput();
        FilterRegistration.Dynamic registration = ctx.addFilter("check-compare-filter", checkCompareFilter);
        registration.addMappingForServletNames(null, true, "compare");
    }

    private void registerSetUserInfoFilter(ServletContext ctx) {
        SetUserInfoFilter setUserFilter = new SetUserInfoFilter();
        FilterRegistration.Dynamic registration = ctx.addFilter("set-info-filter", setUserFilter);
        registration.addMappingForServletNames(null, true, "input");
    }

    private void registerInputServlet(ServletContext ctx) {
        InputServlet inputServlet = new InputServlet();
        ServletRegistration.Dynamic registration = ctx.addServlet("input", inputServlet);
        registration.addMapping("/input");
    }

    private void registerLoginServlet(ServletContext ctx) {
        LoginServlet loginServlet = new LoginServlet();
        ServletRegistration.Dynamic registration = ctx.addServlet("login", loginServlet);
        registration.addMapping("");
    }

    private void registerCompareServlet(ServletContext ctx) throws IOException {
        ReconUseCase<WebRequest, List<String>> useCase = getWebReconcileUseCase();
        CompareServlet compareServlet = new CompareServlet(useCase);
        ServletRegistration.Dynamic registration = ctx.addServlet("compare", compareServlet);
        // TODO use Files.createTempDirectory ..Done
        registration.setMultipartConfig(new MultipartConfigElement(Files.createTempFile("file", "tmp").toString()));
        registration.addMapping("/compare");
    }

    private ReconUseCase<WebRequest, List<String>> getWebReconcileUseCase() {
        Validator validator = new RequestValidator();
        return new WebReconUseCase(validator);
    }

    private void registerDownloadServlet(ServletContext ctx) {
        DownloadServlet downloadServlet = new DownloadServlet();
        ServletRegistration.Dynamic registration = ctx.addServlet("download", downloadServlet);
        registration.addMapping("/download");
    }

    private void registerRenderingServlet(ServletContext ctx) {
        RenderingResultApi renderServlet = new RenderingResultApi();
        ServletRegistration.Dynamic registration = ctx.addServlet("render", renderServlet);
        registration.addMapping("/render");
    }
}
