package com.progressoft;

import javax.servlet.http.Part;

public class FileRequest {
    private Part file;
    private String fileName;
    private String fileType;

    public Part getFile() {
        return file;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public FileRequest(Part file, String fileName, String fileType) {
        this.file = file;
        this.fileName = fileName;
        this.fileType = fileType;
    }
}
