package com.progressoft;
import com.progressoft.reconciliation.ReconRequest;
import org.apache.commons.io.IOUtils;

import javax.servlet.http.Part;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class WebRequest implements ReconRequest {
    private String resultFormat;
    private FileRequest sourceFile;
    private FileRequest targetFile;
    public WebRequest(String resultFormat, FileRequest sourceFile, FileRequest targetFile) {
        this.resultFormat = resultFormat;
        this.sourceFile = sourceFile;
        this.targetFile = targetFile;
    }
    @Override
    public String getSourceFile() {
        return getTempFilePath(sourceFile.getFile() ,sourceFile.getFileName());
    }
    @Override
    public String getSourceType() {
        return sourceFile.getFileType();
    }
    @Override
    public String getTargetFile() {
        return getTempFilePath(targetFile.getFile() , targetFile.getFileName());
    }
    @Override
    public String getTargetType() {
        return targetFile.getFileType();
    }
    public String getResultFormat(){
        return resultFormat;
    }
    private String getTempFilePath(Part file , String fileName){
       String actualType = file.getContentType().split("/")[1];
        try {
            Path tempFile = Files.createTempFile(fileName,"."+actualType);
            IOUtils.copy(file.getInputStream() , Files.newOutputStream(tempFile));
            return tempFile.toString();
        } catch (IOException e) {
            throw new RuntimeException("Error while reading uploaded files!");
        }
    }
}
