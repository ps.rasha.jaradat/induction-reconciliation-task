package com.progressoft;

import com.progressoft.generator.GeneratorFactory;
import com.progressoft.reader.inputReader.FileReader;
import com.progressoft.reader.inputReader.ReaderFactory;
import com.progressoft.reconcileUseCase.ReconUseCase;
import com.progressoft.reconciliation.ReconCompareResult;
import com.progressoft.reconciliation.ResponseGenerator;
import com.progressoft.reconciliation.TransactionRecord;
import com.progressoft.validator.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

public class WebReconUseCase extends ReconUseCase<WebRequest, List<String>> {
    private String resultType;
    private Validator<WebRequest> validator;

    public WebReconUseCase(Validator<WebRequest> validator) {
        this.validator = validator;
    }

    @Override
    protected void validateRequest(WebRequest request) {
        validator.isValid(request);
        this.resultType = request.getResultFormat();
    }

    @Override
    protected Map<String, ? extends TransactionRecord> readSource(WebRequest request) {
        return readFile(request.getSourceType(), request.getSourceFile());
    }

    @Override
    protected Map<String, ? extends TransactionRecord> readTarget(WebRequest request) {
        return readFile(request.getTargetType(), request.getTargetFile());
    }

    @Override
    protected List<String> generateResponse(ReconCompareResult result) {
        try {
            Path tempDir = Files.createTempDirectory("tmpDir");
            ResponseGenerator<String> resultGenerator =
                    (ResponseGenerator<String>) GeneratorFactory.getGenerator(resultType, tempDir.toString());
            List<String> resultFiles = resultGenerator.generate(result);
            addDirPathToList(tempDir, resultFiles);
            return resultFiles;
        } catch (IOException e) {
            // TODO swallowing exception ..Done
            throw new IllegalStateException("Error while saving result files..");
        }
    }

    private void addDirPathToList(Path tempDir, List<String> resultFiles) {
        resultFiles.add(tempDir.toString());
    }

    private Map<String, ? extends TransactionRecord> readFile(String type, String filePath) {
        FileReader fileReader = ReaderFactory.getReader(type);
        return fileReader.read(Paths.get(filePath));
    }
}
