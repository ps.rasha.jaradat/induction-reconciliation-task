<%@taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<div class="tab panel panel-default"><h3>  Upload Source File:</h3></br><tags:file-input type="source"/></div>
<div class="tab panel panel-default"><h3>  Upload Target File:</h3></br><tags:file-input type="target"/></div>
<div class="tab panel-group">
    <tags:show-user-input/>
    <tags:result-format/>
</div>