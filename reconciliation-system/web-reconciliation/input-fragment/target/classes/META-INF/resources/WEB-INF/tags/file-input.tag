<%@attribute name="type" rtexprvalue="true" required="true" type="java.lang.String" %>
<div class="form-group ">
    <label for="${type}-name">${type.toUpperCase()} File Name</label>
    <input type="text" class="form-control" id="${type}-name" name="${type}FileName" placeholder="enter ${type} file name"
              oninput="this.className = ''">
</div>
<div class="form-group ">
    <label for="${type}-type">${type.toUpperCase()} File Type</label>
    <input type="text" class="form-control" id="${type}-type" name="${type}FileType" placeholder="enter ${type} file type"
              oninput="this.className = ''">
</div>
<div class="form-group ">
    <label for="${type}-file">${type.toUpperCase()} File </label>
    <input type="file" class="form-control" id="${type}-file" name="${type}File" placeholder="choose ${type} file"
              oninput="this.className = ''">
</div>