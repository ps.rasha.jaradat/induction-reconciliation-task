<div style="overflow:auto;">
    <div style="float:right;">
        <button class="btn btn-primary" type="button" id="cancelBtn"  onclick="cancel()">Cancel</button>
        <button  class="btn btn-primary" type="button" id="prevBtn"  onclick="nextPrev(-1)">Previous</button>
        <button class="btn btn-primary" type="button" id="nextBtn"  onclick="nextPrev(1)">Next</button>
    </div>
</div>
<!-- Circles which indicates the steps of the form: -->
<div style="text-align:center;margin-top:40px;">
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
</div>