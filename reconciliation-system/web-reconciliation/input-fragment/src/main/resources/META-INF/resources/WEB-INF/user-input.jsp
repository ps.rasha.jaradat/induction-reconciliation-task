<%@taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<html>
<tags:head/>
<body>
<form id="compareForm" method="POST" action="${pageContext.request.contextPath}/compare" enctype="multipart/form-data">
    <tags:panels/>
    <tags:wizard-btn/>
    <tags:steps/>
</form>
</body>
<tags:footer/>
</html>
