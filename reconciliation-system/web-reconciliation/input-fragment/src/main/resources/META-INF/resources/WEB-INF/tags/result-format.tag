<div class="dropdown">
    <label for="result-format">Choose Result Files Format</label></br>
    <select class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="result-format" name="resultFormat" >
        <span class="caret"></span>
        <option>CSV</option>
        <option>JSON</option>
    </select>
</div>