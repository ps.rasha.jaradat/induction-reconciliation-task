var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
    var x = document.getElementsByClassName("tab");
    x[n].style.display = "block";
    if (n == 0) {
        document.getElementById("prevBtn").style.display = "none";
    } else {
        document.getElementById("prevBtn").style.display = "inline";
    }
    if(n != (x.length - 1)){
        document.getElementById("cancelBtn").style.display ="none";
    }

    if (n == (x.length - 1)) {
        document.getElementById("nextBtn").innerHTML = "compare";
        document.getElementById("cancelBtn").style.display = "inline";
        document.getElementById("src-name-ren").innerHTML = $("#source-name").val();
        document.getElementById("src-type-ren").innerHTML = $("#source-type").val();
        document.getElementById("trg-name-ren").innerHTML = $("#target-name").val();
        document.getElementById("trg-type-ren").innerHTML = $("#target-type").val();

    } else {
        document.getElementById("nextBtn").innerHTML = "Next";
    }
    fixStepIndicator(n)
}
function cancel() {
    var x = document.getElementsByClassName("tab");
    x[currentTab].style.display = "none";
    currentTab = 0;
    showTab(currentTab);
}
function nextPrev(n) {
    var x = document.getElementsByClassName("tab");
    if (n == 1 && !validateForm()) return false;
    x[currentTab].style.display = "none";
    currentTab = currentTab + n;
    if (currentTab >= x.length) {
        document.getElementById("compareForm").submit();
        return false;
    }
    showTab(currentTab);
}


function validateForm() {
    var x, y, i, valid = true;
    x = document.getElementsByClassName("tab");
    y = x[currentTab].getElementsByTagName("input");
    for (i = 0; i < y.length; i++) {
        if (y[i].value == "") {
            y[i].className += " invalid";
            valid = false;
        }
    }
    if (valid) {
        document.getElementsByClassName("step")[currentTab].className += " finish";
    }
    return valid;
}

function fixStepIndicator(n) {
    var i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
        x[i].className = x[i].className.replace(" active", "");
    }
    x[n].className += " active";
}
