package com.progressoft;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.PrintWriter;
import java.io.StringWriter;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class InputServletTest {
    HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
    HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    RequestDispatcher requestDispatcher = Mockito.mock(RequestDispatcher.class);
    HttpSession session = Mockito.mock(HttpSession.class);

    @Test
    public void whenDoGet_thenForwardToInputPage() {
        try {
            when(response.getWriter()).thenReturn(new PrintWriter(new StringWriter()));
            when(request.getSession()).thenReturn(session);
            when(request.getRequestDispatcher("WEB-INF/user-input.jsp")).thenReturn(requestDispatcher);
            new InputServlet().doGet(request, response);
            verify(requestDispatcher).forward(request, response);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void whenDoPost_thenRedirect() {
        try {
            when(request.getRequestURI()).thenReturn("/input");
            response.sendRedirect("http://localhost:8080/recon/input");
            verify(response).sendRedirect("http://localhost:8080/recon/input");
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
}
