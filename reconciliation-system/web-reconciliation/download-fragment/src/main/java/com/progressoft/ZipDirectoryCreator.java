package com.progressoft;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipDirectoryCreator {

    public byte[] createZipFile(List<String> filePaths) throws IOException {
        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
             ZipOutputStream zipOutputStream = new ZipOutputStream(byteArrayOutputStream)) {
            for (String fileName : filePaths) {
                addFileToZipDirectory(zipOutputStream, fileName);
            }
            zipOutputStream.flush();
            byteArrayOutputStream.flush();
            zipOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        }
    }

    private void addFileToZipDirectory(ZipOutputStream zipOutputStream, String fileName) throws IOException {
        byte[] bytes = new byte[2048];
        try (FileInputStream fileInputStream = new FileInputStream(fileName);
             BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream)) {
            zipOutputStream.putNextEntry(new ZipEntry(fileName.split("/")[4]));
            int bytesRead;
            while ((bytesRead = bufferedInputStream.read(bytes)) != -1) {
                zipOutputStream.write(bytes, 0, bytesRead);
            }
            zipOutputStream.closeEntry();
        }
    }

}

