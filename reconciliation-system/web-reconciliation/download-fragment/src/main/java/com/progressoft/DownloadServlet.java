package com.progressoft;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DownloadServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            List<String> result = getDirContentFiles(request);
            checkResponseSize(result);
            // TODO you should stop after sendError ..Done
            setResponseHeaders(response);
            sendResponse(response, result);
        } catch (IOException e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Un Expected Error occurred");
        }
    }
    private void sendResponse(HttpServletResponse response, List<String> result) throws IOException {
        ServletOutputStream servletOutputStream = response.getOutputStream();
        ZipDirectoryCreator creator = new ZipDirectoryCreator();
        servletOutputStream.write(creator.createZipFile(result));
        servletOutputStream.flush();
    }
    private void setResponseHeaders(HttpServletResponse response) {
        response.setContentType("application/zip");
        response.setHeader("Content-Disposition", "attachment; filename=recon_result.zip");
    }
    private List<String> getDirContentFiles(HttpServletRequest request) throws IOException {
        String path = (String) request.getSession().getAttribute("resultDir");
        Stream<Path> walk = Files.walk(Paths.get(path));
        return walk.filter(Files::isRegularFile).map(x -> x.toString()).collect(Collectors.toList());
    }
    private void checkResponseSize(List<String> result) throws IOException {
        if (result.size() != 3) {
            throw new IOException();
        }
    }
}

