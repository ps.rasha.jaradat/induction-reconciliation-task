package com.progressoft;

import com.google.gson.Gson;
import com.progressoft.entities.MatchingRecord;
import com.progressoft.entities.NotMatchingRecord;
import com.progressoft.reader.resultReader.ResultReader;
import com.progressoft.reader.resultReader.ResultReaderFactory;
import org.json.simple.parser.ParseException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

// TODO rename to API or Rest Service .. Done
public class RenderingResultApi extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String format = (String) req.getSession().getAttribute("resultFormat");
        String fileType = req.getParameter("fileType");
        String filePath = (String) req.getSession().getAttribute(fileType);
        resp.setContentType("application/json");
        sendResponse(resp, format, fileType, filePath);
    }

    private void sendResponse(HttpServletResponse resp, String format, String fileType, String filePath) throws IOException {
        try {
            ResultReader reader = ResultReaderFactory.getReader(format, fileType);
            if ("matching".equals(fileType))
                sendResponseWithMatching(resp, reader, filePath);
            else
                sendResponseWithNotMatching(resp, reader, filePath);
        } catch (Exception e) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Un expected error occurred!");
        }
    }

    private void sendResponseWithNotMatching(HttpServletResponse resp, ResultReader reader, String filePath) throws IOException, ParseException {
        List<NotMatchingRecord> notMatchingRecords = reader.readNotMatch(filePath);
        new Gson().toJson(notMatchingRecords, resp.getWriter());
    }

    private void sendResponseWithMatching(HttpServletResponse resp, ResultReader reader, String filePath) throws IOException, ParseException {
        List<MatchingRecord> matchingRecords = reader.readMatch(filePath);
        new Gson().toJson(matchingRecords, resp.getWriter());
    }
}
