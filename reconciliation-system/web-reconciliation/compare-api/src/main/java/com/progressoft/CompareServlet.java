package com.progressoft;
import com.progressoft.reconcileUseCase.ReconUseCase;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

public class CompareServlet extends HttpServlet {
    private ReconUseCase<WebRequest, List<String>> useCase;

    public CompareServlet(ReconUseCase<WebRequest, List<String>> useCase) {
        this.useCase = useCase;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        if (Objects.isNull(req.getSession().getAttribute("resultDir"))) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "No Data input to Compare !");
        }
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/compare-result.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            // TODO should return an Object Result
            List<String> list = doCompare(req);
            setSessionAttribute(req, list);
            resp.sendRedirect(req.getContextPath() + "/compare");
        } catch (Exception e) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
        }
    }

    private void setSessionAttribute(HttpServletRequest req, List<String> list) {
        req.getSession().setAttribute("resultFormat", req.getParameter("resultFormat"));
        req.getSession().setAttribute("matching", list.get(0));
        req.getSession().setAttribute("miss_matching", list.get(1));
        req.getSession().setAttribute("missing", list.get(2));
        req.getSession().setAttribute("resultDir", list.get(3));
    }

    private List<String> doCompare(HttpServletRequest req) throws IOException, ServletException {
        WebRequest webRequest = getWebRequest(req);
        return useCase.process(webRequest);
    }

    private WebRequest getWebRequest(HttpServletRequest req) throws IOException, ServletException {
        String resultFormat = req.getParameter("resultFormat");
        FileRequest source = setSourceFile(req);
        FileRequest target = setTargetFile(req);
        return new WebRequest(resultFormat, source, target);
    }

    private FileRequest setSourceFile(HttpServletRequest req) throws IOException, ServletException {
        Part sourceFile = req.getPart("sourceFile");
        String sourceName = req.getParameter("sourceFileName");
        String sourceType = req.getParameter("sourceFileType");
        return new FileRequest(sourceFile, sourceName, sourceType);
    }

    private FileRequest setTargetFile(HttpServletRequest req) throws IOException, ServletException {
        Part targetFile = req.getPart("targetFile");
        String targetName = req.getParameter("targetFileName");
        String targetType = req.getParameter("targetFileType");
        return new FileRequest(targetFile, targetName, targetType);
    }
}
