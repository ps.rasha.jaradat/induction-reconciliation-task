<%@page contentType="text/javascript"%>
    $(function () {
        $.ajax({
            url: "${pageContext.request.contextPath}/render?fileType=matching",
            method: "GET",
            contentType: "application/json",
            success: function (data) {
                var selector = $("#match tbody");
                var i = 0;
                $.each(data, function (i, record) {
                    selector.append(
                        "<tr>"
                        + "<td>" + (i + 1) + "</td>"
                        + "<td>" + record.reference + "</td>"
                        + "<td>" + record.amount + "</td>"
                        + "<td>" + record.currencyCode + "</td>"
                        + "<td>" + record.date + "</td>"
                        + "</tr>");
                })
            }
        });
        $.ajax({
            url: "${pageContext.request.contextPath}/render?fileType=missing",
            method: "GET",
            success: function (data) {
                var i = 0;
                var selector = $("#missing tbody");
                $.each(data, function (i, record) {
                    selector.append(
                        "<tr>"
                        + "<td>" + (i + 1) + "</td>"
                        + "<td>" + record.from + "</td>"
                        + "<td>" + record.reference + "</td>"
                        + "<td>" + record.amount + "</td>"
                        + "<td>" + record.currencyCode + "</td>"
                        + "<td>" + record.date + "</td>"
                        + "</tr>");

                })
            }
        });
        $.ajax({
            url: "${pageContext.request.contextPath}/render?fileType=miss_matching",
            method: "GET",
            success: function (data) {
                var i = 0;
                var selector = $("#miss-match tbody");
                $.each(data, function (i, record) {
                    selector.append(
                        "<tr>"
                        + "<td>" + (i + 1) + "</td>"
                        + "<td>" + record.from + "</td>"
                        + "<td>" + record.reference + "</td>"
                        + "<td>" + record.amount + "</td>"
                        + "<td>" + record.currencyCode + "</td>"
                        + "<td>" + record.date + "</td>"
                        + "</tr>");

                })
            }
        });
    });