<%@taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<tags:compare-head/>
<body>
<div class="container">
    <h2>Reconciliation Result</h2>
   <tags:tab-nav/>
   <tags:tab-content/>
</div>
<tags:compare-btn/>
</body>
<tags:compare-footer/>
</html>


