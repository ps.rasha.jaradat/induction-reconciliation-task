<div class="tab-content">
    <div id="match" class="tab-pane fade in active">
        <table>
            <thead>
            <th>#</th>
            <th>ID</th>
            <th>Amount</th>
            <th>Currency Code</th>
            <th>Date</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <div id="missing" class="tab-pane fade">
        <table>
            <thead>
            <th>#</th>
            <th>From</th>
            <th>ID</th>
            <th>Amount</th>
            <th>Currency Code</th>
            <th>Date</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <div id="miss-match" class="tab-pane fade">
        <table>
            <thead>
            <th>#</th>
            <th>From</th>
            <th>ID</th>
            <th>Amount</th>
            <th>Currency Code</th>
            <th>Date</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
