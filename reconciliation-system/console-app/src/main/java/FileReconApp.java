import com.progressoft.reconcileUseCase.ReconUseCase;
import com.progressoft.reconciliation.ReconRequest;
import com.progressoft.fileSystemUseCase.FileReconciliationUseCase;
import com.progressoft.validator.RequestValidator;
import com.progressoft.validator.Validator;

import java.util.List;
import java.util.Scanner;

public class FileReconApp {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(">> Enter source file location:");
        String sourceFile = sc.next();
        System.out.println(">> Enter source file format:");
        String sourceType = sc.next();
        System.out.println(">> Enter target file location:");
        String targetFile = sc.next();
        System.out.println(">> Enter target file format:");
        String targetType = sc.next();
        Validator<ReconRequest> validator = new RequestValidator();
        ConsoleRequest request = new ConsoleRequest(sourceFile, sourceType, targetFile, targetType);
        ReconUseCase<ReconRequest, List<String>> reconciliation = new FileReconciliationUseCase(validator);
        List<String> result = reconciliation.process(request);
        printResult(result);
    }

    private static void printResult(List<String> result) {
        System.out.println("Reconciliation finished.");
        System.out.println("Result files are available in :");
        System.out.println("Matching Transactions : " + result.get(0));
        System.out.println("MissMatching Transactions : " + result.get(1));
        System.out.println("Missing Transactions : " + result.get(2));
    }
}

/*
/home/user/induction-reconciliation-task/sample-files/input-files/bank-transactions.csv
csv
/home/user/induction-reconciliation-task/sample-files/input-files/online-banking-transactions.json
json

*/
