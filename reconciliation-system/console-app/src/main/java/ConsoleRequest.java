import com.progressoft.reconciliation.ReconRequest;
public class ConsoleRequest implements ReconRequest {
    private String sourceFile;
    private String targetFile;
    private String sourceType;
    private String targetType;

    public ConsoleRequest(String sourceFile, String sourceType, String targetFile, String targetType) {
        this.sourceFile = sourceFile;
        this.sourceType = sourceType;
        this.targetFile = targetFile;
        this.targetType = targetType;
    }
    @Override
    public String getSourceFile() {
        return sourceFile;
    }
    @Override
    public String getSourceType() {
        return sourceType;
    }
    @Override
    public String getTargetFile() {
        return targetFile;
    }
    @Override
    public String getTargetType() {
        return targetType;
    }
}
