package com.progressoft.reader.resultReader;

import com.progressoft.entities.MatchingRecord;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MatchingJsonResultReader implements ResultReader {
    private static MetaFile json_Matching_Meta = new MetaFile() {
        @Override
        public int getId() {
            return 1;
        }

        @Override
        public int getAmount() {
            return 2;
        }

        @Override
        public int getCurrencyCode() {
            return 3;
        }

        @Override
        public int getDate() {
            return 0;
        }

        @Override
        public int getSource() {
            return 5;
        }


    };
    @Override
    public List<MatchingRecord> readMatch(String path){
        try (java.io.FileReader reader = new FileReader(path)) {
            return getRecordFromFile(reader);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException("file path you are trying to read not found, try another one");
        } catch (IOException e) {
            throw new IllegalStateException("Error while reading json file :" + path);
        } catch (ParseException e) {
            throw new RuntimeException("Error while parsing json,something wrong with file format");
        }
    }
    private List<MatchingRecord> getRecordFromFile(FileReader reader) throws IOException, ParseException {
        List<MatchingRecord> matchingRecords = new ArrayList<>();
        JSONParser jsonParser = new JSONParser();
        List<JSONObject> records = (List<JSONObject>) jsonParser.parse(reader);

        for (JSONObject record : records) {

            Object[] values = record.values().toArray();

            String date = (String) values[json_Matching_Meta.getDate()];
            String id = (String) values[json_Matching_Meta.getId()];
            BigDecimal amount = new BigDecimal(values[json_Matching_Meta.getAmount()].toString());
            String currencyCode = (String) values[json_Matching_Meta.getCurrencyCode()];

            MatchingRecord matchingRecord = new MatchingRecord(date, id, amount, currencyCode);
            matchingRecords.add(matchingRecord);
        }
        return matchingRecords;
    }
}

