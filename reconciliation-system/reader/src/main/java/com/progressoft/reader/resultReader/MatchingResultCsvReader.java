package com.progressoft.reader.resultReader;
import com.progressoft.entities.MatchingRecord;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MatchingResultCsvReader implements ResultReader {
    private MetaFile csv_Matching_Meta = new MetaFile(){
        @Override
        public int getId() {
            return 0;
        }

        @Override
        public int getAmount() {
            return 1;
        }

        @Override
        public int getCurrencyCode() {
            return 2;
        }

        @Override
        public int getDate() {
            return 3;
        }

        @Override
        public int getSource() {
            return 0;
        }

    };

    @Override
    public List<MatchingRecord> readMatch(String  filePath){
        List<MatchingRecord> matchingRecords = new ArrayList<>();
        try (BufferedReader in = new BufferedReader(new FileReader(filePath))) {
            in.readLine();
            while (in.ready()) {
                String[] record = in.readLine().split(",");
                MatchingRecord matchingRecord = new MatchingRecord(record[csv_Matching_Meta.getDate()],record[csv_Matching_Meta.getId()]
                        , new BigDecimal(record[csv_Matching_Meta.getAmount()]), record[csv_Matching_Meta.getCurrencyCode()]);
                matchingRecords.add(matchingRecord);
            }
        } catch (IOException e) {
            throw new IllegalStateException("file path you are trying to read not found, try another one");
        }
        return matchingRecords;
    }
}
