package com.progressoft.reader.exceptions;

public class IntegrityException extends RuntimeException {
    public IntegrityException(String message) {
        super(message);
    }
}
