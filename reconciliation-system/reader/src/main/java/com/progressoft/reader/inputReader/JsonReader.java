package com.progressoft.reader.inputReader;

import com.progressoft.reader.exceptions.AmountFormatException;
import com.progressoft.reader.exceptions.DateFormatException;
import com.progressoft.reader.exceptions.IntegrityException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.progressoft.entities.JsonRecord;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.*;

public class JsonReader implements FileReader {
    @Override
    public Map<String, JsonRecord> read(Path path) {
        Map<String, JsonRecord> result = new HashMap<>();
        JsonConsumer jsonConsumer=new JsonConsumer(result);
        JSONParser jsonParser = new JSONParser();
        try (java.io.FileReader reader = new java.io.FileReader(path.toString())) {
            int transactionCounter = 0;
            throwIfEmpty(reader);
            List<JSONObject> records = (List<JSONObject>) jsonParser.parse(reader);
            for (JSONObject record : records) {
                transactionCounter++;
                throwIfHeaderIsWrong(record, transactionCounter);
                jsonConsumer.accept(jsonConsumer.map(record));
            }
            throwIfNotUnique(transactionCounter , result);
        } catch (ParseException e) {
            throw new RuntimeException("Error while parsing json,something wrong with file format");
        } catch (IOException e) {
            throw new IllegalStateException("file path you are trying to read not found, try another one",e);
        }
        return result;
    }

    private void throwIfEmpty(java.io.FileReader reader) throws IOException {
        if(! reader.ready())
            throw new IllegalArgumentException("Json file if Empty");
    }

    private void throwIfNotUnique(int transactionCounter,Map result) {
        if (transactionCounter != result.size())
            throw new IntegrityException("It looks like there is a repeated rows,or there is a transaction id that used in more than one row");
    }
    private List<String> validHeaders = Arrays.asList("date", "reference", "amount", "currencyCode", "purpose");
    private void throwIfHeaderIsWrong(JSONObject record, int recordNumber) {
        Object[] headers = record.keySet().toArray();
        for (int i = 0; i < headers.length; i++) {
            String curHeader = headers[i].toString();
            if (notValidHeader(curHeader))
                throw new IllegalArgumentException("Record : " + recordNumber + " with inCorrect header, key : '" + curHeader + "'");
        }
    }
    private boolean notValidHeader(String curHeader) {
        return !validHeaders.contains(curHeader);
    }
}

class JsonConsumer implements FileConsumer<JSONObject, JsonRecord> {
    private Map<String, JsonRecord> recordMap = new HashMap<>();
    JsonConsumer(Map<String, JsonRecord> result) {
        this.recordMap = result;
    }
    @Override
    public JsonRecord map(JSONObject fileRecord) {
        Object[] values = fileRecord.values().toArray();
        LocalDate validDate = validateDate(values[0].toString());
        BigDecimal validAmount = validateAmount(values[2].toString(), values[4].toString());
        return new JsonRecord(validDate, values[1].toString(), validAmount, values[4].toString(), values[3].toString());
    }
    @Override
    public void accept(JsonRecord jsonRecord) {
        recordMap.put(jsonRecord.getTransactionId(), jsonRecord);
    }
    private BigDecimal validateAmount(String amount, String currency) {
        int scale = Currency.getInstance(currency.toUpperCase()).getDefaultFractionDigits();
        try {
            return new BigDecimal(amount).setScale(scale);
        } catch (NumberFormatException e) {
            throw new AmountFormatException("Amount '" + amount + "' is not correct");
        }
    }
    private LocalDate validateDate(String date) {
        if (!date.matches("^(\\d{2}\\/\\d{2}\\/\\d{4})$"))
            throw new DateFormatException("date : " + date + " has wrong format, use DD/MM/YYYY");
        String[] dateArray = date.split("/");
        return LocalDate.of(Integer.parseInt(dateArray[2]), Integer.parseInt(dateArray[1]), Integer.parseInt(dateArray[0]));

    }
}

