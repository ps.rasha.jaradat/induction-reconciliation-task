package com.progressoft.reader.inputReader;
import com.progressoft.reconciliation.TransactionRecord;

import java.nio.file.Path;
import java.util.Map;

public interface FileReader {
     Map<String, ? extends TransactionRecord> read(Path path);
}

