package com.progressoft.reader.resultReader;

public class ResultReaderFactory {
    private ResultReaderFactory() {
    }

    public static ResultReader getReader(String resultFormat, String type) {
        if ("matching".equalsIgnoreCase(type)) {
            if ("csv".equalsIgnoreCase(resultFormat))
                return new MatchingResultCsvReader();
            return new MatchingJsonResultReader();
        } else if (("miss_matching".equalsIgnoreCase(type)) || ("missing".equalsIgnoreCase(type))) {
            if ("csv".equalsIgnoreCase(resultFormat))
                return new NotMatchingCsvResultReader();
            return new NotMatchingJsonResultReader();
        }
        return null;
    }
}
