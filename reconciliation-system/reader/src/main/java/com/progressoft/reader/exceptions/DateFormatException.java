package com.progressoft.reader.exceptions;

public class DateFormatException extends RuntimeException {
    public DateFormatException(String message) {
        super(message);
    }
}
