package com.progressoft.reader.resultReader;

import com.progressoft.entities.NotMatchingRecord;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class NotMatchingJsonResultReader implements ResultReader {
    private MetaFile json_Not_Matching_Meta = new MetaFile() {
        @Override
        public int getId() {
            return 1;
        }

        @Override
        public int getAmount() {
            return 2;
        }

        @Override
        public int getCurrencyCode() {
            return 4;
        }

        @Override
        public int getDate() {
            return 0;
        }

        @Override
        public int getSource() {
            return 3;
        }
    };

    @Override
    public List<NotMatchingRecord> readNotMatch(String path) {

        try (java.io.FileReader reader = new FileReader(path)) {
            return getNotMatchingRecords(reader);
        } catch (
                FileNotFoundException e) {
            throw new IllegalStateException("file path you are trying to read not found, try another one");
        } catch (IOException e) {
            throw new IllegalStateException("Error while reading json file :" + path);
        } catch (ParseException e) {
            throw new RuntimeException("Error while parsing json,something wrong with file format");
        }
    }

    private List<NotMatchingRecord> getNotMatchingRecords(FileReader reader) throws IOException, ParseException {
        JSONParser jsonParser = new JSONParser();
        List<JSONObject> records = (List<JSONObject>) jsonParser.parse(reader);
        List<NotMatchingRecord> notMatchingRecords = new ArrayList<>();
        for (JSONObject record : records) {
            Object[] values = record.values().toArray();
            String value = (String) values[json_Not_Matching_Meta.getDate()];
            String from = (String) values[json_Not_Matching_Meta.getSource()];
            String reference = (String) values[json_Not_Matching_Meta.getId()];
            BigDecimal amount = new BigDecimal(values[json_Not_Matching_Meta.getAmount()].toString());
            String currencyCode = (String) values[json_Not_Matching_Meta.getCurrencyCode()];
            NotMatchingRecord notMatchingRecord = new NotMatchingRecord(from,value,reference,amount,currencyCode);
            notMatchingRecords.add(notMatchingRecord);
        }
        return notMatchingRecords;
    }
}





