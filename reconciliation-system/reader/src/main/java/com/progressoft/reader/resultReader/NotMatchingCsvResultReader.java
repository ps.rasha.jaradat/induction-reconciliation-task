package com.progressoft.reader.resultReader;

import com.progressoft.entities.NotMatchingRecord;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class NotMatchingCsvResultReader implements ResultReader {
    private MetaFile csv_Not_Matching_Meta = new MetaFile() {
        @Override
        public int getId() {
            return 1;
        }

        @Override
        public int getAmount() {
            return 2;
        }

        @Override
        public int getCurrencyCode() {
            return 3;
        }

        @Override
        public int getDate() {
            return 4;
        }

        @Override
        public int getSource() {
            return 0;
        }

    };

    @Override
    public List<NotMatchingRecord> readNotMatch(String path) {
        try (BufferedReader in = new BufferedReader(new FileReader(path))) {
            skipHeader(in);
            List<NotMatchingRecord> notMatchingRecords = getNotMatchingRecords(in);
            return notMatchingRecords;
        } catch (IOException e) {
            throw new IllegalStateException("file path you are trying to read not found, try another one");
        }
    }

    private List<NotMatchingRecord> getNotMatchingRecords(BufferedReader in) throws IOException {
        List<NotMatchingRecord> notMatchingRecords = new ArrayList<>();
        while (in.ready()) {
            String[] record = in.readLine().split(",");
            String from = record[csv_Not_Matching_Meta.getSource()];
            String date = record[csv_Not_Matching_Meta.getDate()];
            String reference = record[csv_Not_Matching_Meta.getId()];
            BigDecimal amount = new BigDecimal(record[csv_Not_Matching_Meta.getAmount()]);
            String currencyCode = record[csv_Not_Matching_Meta.getCurrencyCode()];
            NotMatchingRecord notMatchingRecord = new NotMatchingRecord(from, date, reference, amount, currencyCode);
            notMatchingRecords.add(notMatchingRecord);
        }
        return notMatchingRecords;
    }

    private void skipHeader(BufferedReader in) throws IOException {
        in.readLine();
    }
}
