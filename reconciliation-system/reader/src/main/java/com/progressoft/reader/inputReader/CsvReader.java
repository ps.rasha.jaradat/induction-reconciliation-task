package com.progressoft.reader.inputReader;

import com.progressoft.entities.CsvRecord;
import com.progressoft.reader.exceptions.AmountFormatException;
import com.progressoft.reader.exceptions.DateFormatException;
import com.progressoft.reader.exceptions.IntegrityException;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;

public class CsvReader implements FileReader {
    private final String CSV_HEADER = "trans unique id,trans description,amount,currecny,purpose,value date,trans type";

    @Override
    public Map<String, CsvRecord> read(Path path) {
        Map<String, CsvRecord> result = new HashMap<>();
        CsvConsumer csvConsumer = new CsvConsumer(result);
        try (BufferedReader bf = Files.newBufferedReader(path)) {
            ValidateFile(path);
            skipHeader(bf);
            int transactionCounter = 0;
            while (bf.ready()) {
                csvConsumer.accept(csvConsumer.map(gedRecord(bf)));
                transactionCounter++;
            }
            throwIfNotUnique(transactionCounter, result);
            return result;
        } catch (IOException e) {
            throw new IllegalStateException("Error occurred while opening csv file, file might not be found!" , e);
        }
    }

    private String[] gedRecord(BufferedReader bf) throws IOException {
        return bf.readLine().split(",");
    }

    private void skipHeader(BufferedReader bf) throws IOException {
        bf.readLine();
    }

    private void throwIfNotUnique(int counter, Map result) {
        if (counter != result.size())
            throw new IntegrityException("It looks like there is a repeated rows,or there is a transaction id that used in more than one row");
    }

    private void ValidateFile(Path path) throws IOException {
        try (BufferedReader bf = Files.newBufferedReader(path)) {
            if (isEmpty(bf))
                throw new IllegalArgumentException("csv File is empty!");
            String header = bf.readLine();
            if (notValidHeader(header))
                throw new IllegalArgumentException("csv File header is not correct it should be : " + CSV_HEADER);
            if (bf.read() == -1)
                throw new IllegalArgumentException("csv File with no record!");
        }
    }

    private boolean notValidHeader(String header) {
        return !CSV_HEADER.equals(header);
    }

    private boolean isEmpty(BufferedReader bf) throws IOException {
        return !bf.ready();
    }
}

class CsvConsumer implements FileConsumer<String[], CsvRecord> {
    private Map<String, CsvRecord> csvRecordMap;

    CsvConsumer(Map csvRecordMap) {
        this.csvRecordMap = csvRecordMap;
    }

    @Override
    public void accept(CsvRecord record) {
        csvRecordMap.put(record.getTransactionId(), record);
    }

    @Override
    public CsvRecord map(String[] record) {
        return new CsvRecord(record[0], validateAmount(record[2], record[3]), record[3], validateDate(record[5]));
    }

    private BigDecimal validateAmount(String amount, String currencyCode) {
        try {
            int scale = Currency.getInstance(currencyCode).getDefaultFractionDigits();
            return new BigDecimal(amount).setScale(scale);
        } catch (NumberFormatException e) {
            throw new AmountFormatException("This amount '" + amount + "' is not number");
        }
    }

    private LocalDate validateDate(String s) {
        if (!(s.matches("^(\\d{4}-\\d{2}-\\d{2})$")))
            throw new DateFormatException("Date '" + s + "' format is not correct, It should be YYYY-MM-DD");
        try {
            String[] date = s.split("-");
            return LocalDate.of(Integer.parseInt(date[0]), Integer.parseInt(date[1]), Integer.parseInt(date[2]));
        } catch (DateTimeException e) {
            throw new DateFormatException("Date '" + s + "' is not correct!");
        }
    }
}


