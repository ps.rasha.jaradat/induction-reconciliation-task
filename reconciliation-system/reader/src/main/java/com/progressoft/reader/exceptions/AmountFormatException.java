package com.progressoft.reader.exceptions;

public class AmountFormatException extends RuntimeException {
    public AmountFormatException(String message) {
        super(message);
    }
}
