package com.progressoft.reader.resultReader;

public interface MetaFile {
     int getId();
     int getAmount();
     int getCurrencyCode();
     int getDate();
     int getSource();

}
