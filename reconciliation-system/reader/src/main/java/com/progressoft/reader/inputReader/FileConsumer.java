package com.progressoft.reader.inputReader;

public interface FileConsumer<FILE_RECORD,TRAN_RECORD> {
    TRAN_RECORD map(FILE_RECORD file_record);
    void accept(TRAN_RECORD record);

}
