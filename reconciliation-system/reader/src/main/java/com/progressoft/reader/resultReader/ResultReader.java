package com.progressoft.reader.resultReader;

import com.progressoft.entities.MatchingRecord;
import com.progressoft.entities.NotMatchingRecord;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public interface ResultReader {
    default List<NotMatchingRecord> readNotMatch(String path) throws IOException, ParseException {
        List<NotMatchingRecord> returnedList = new ArrayList<>();
        return returnedList;
    }

    default List<MatchingRecord> readMatch(String path) throws IOException, ParseException {
        List<MatchingRecord> returnedList = new ArrayList<>();
        return returnedList;
    }
}
