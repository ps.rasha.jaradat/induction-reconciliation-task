package com.progressoft.reader.inputReader;

public class ReaderFactory {
    private ReaderFactory() {
    }

    public static FileReader getReader(String type) {
        if ("csv".equals(type.toLowerCase()))
            return new CsvReader();
        else

            return new JsonReader();

    }
}
