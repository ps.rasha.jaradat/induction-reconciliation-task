package com.progressoft;
import com.progressoft.reader.resultReader.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ResultReaderFactoryTest {
    @Test
    public void givenCsvFormatAndMatching_whenGetReader_thenReturnCsvReader() {
        Assertions.assertEquals(
                ResultReaderFactory.getReader("csv" , "matching").getClass().getName(), MatchingResultCsvReader.class.getName());
        Assertions.assertEquals(
                ResultReaderFactory.getReader("CSV" , "matching").getClass().getName(), MatchingResultCsvReader.class.getName());
    }
    @Test
    public void givenJsonFormatAndMatching_whenGetReader_thenReturnJsonReader() {
        Assertions.assertEquals(
                ResultReaderFactory.getReader("json" , "matching").getClass().getName(), MatchingJsonResultReader.class.getName());
        Assertions.assertEquals(
                ResultReaderFactory.getReader("JSON" , "matching").getClass().getName(), MatchingJsonResultReader.class.getName());
    }
    @Test
    public void givenCsvFormatAndMissMatching_whenGetReader_thenReturnCsvReader() {
        Assertions.assertEquals(
                ResultReaderFactory.getReader("csv","miss_matching").getClass().getName(), NotMatchingCsvResultReader.class.getName());
        Assertions.assertEquals(
                ResultReaderFactory.getReader("CSV","miss_matching").getClass().getName(), NotMatchingCsvResultReader.class.getName());
    }

    @Test
    public void givenJsonFormatAndMissMatching_whenGetReader_thenReturnJsonReader() {
        Assertions.assertEquals(
                ResultReaderFactory.getReader("json" , "miss_matching").getClass().getName(), NotMatchingJsonResultReader.class.getName());
        Assertions.assertEquals(
                ResultReaderFactory.getReader("json" , "miss_matching").getClass().getName(), NotMatchingJsonResultReader.class.getName());
    }
    @Test
    public void givenCsvFormatAndMissing_whenGetReader_thenReturnCsvReader() {
        Assertions.assertEquals(
                ResultReaderFactory.getReader("csv","missing").getClass().getName(), NotMatchingCsvResultReader.class.getName());
        Assertions.assertEquals(
                ResultReaderFactory.getReader("CSV","missing").getClass().getName(), NotMatchingCsvResultReader.class.getName());
    }

    @Test
    public void givenJsonFormatAndMissing_whenGetReader_thenReturnJsonReader() {
        Assertions.assertEquals(
                ResultReaderFactory.getReader("json" , "missing").getClass().getName(), NotMatchingJsonResultReader.class.getName());
        Assertions.assertEquals(
                ResultReaderFactory.getReader("json" , "missing").getClass().getName(), NotMatchingJsonResultReader.class.getName());
    }
}
