package com.progressoft;

import com.progressoft.reader.inputReader.CsvReader;
import com.progressoft.reader.inputReader.JsonReader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import com.progressoft.reader.inputReader.ReaderFactory;


public class ReaderFactoryTest {
    @Test
    public void givenCsvType_whenGetReader_thenReturnCsvReader() {
        Assertions.assertEquals(
                ReaderFactory.getReader("csv").getClass().getName(), CsvReader.class.getName());
        Assertions.assertEquals(
                ReaderFactory.getReader("CSV").getClass().getName(), CsvReader.class.getName());
    }

    @Test
    public void givenJsonType_whenGetReader_thenReturnJsonReader() {
        Assertions.assertEquals(
                ReaderFactory.getReader("json").getClass().getName(), JsonReader.class.getName());
        Assertions.assertEquals(
                ReaderFactory.getReader("JSON").getClass().getName(), JsonReader.class.getName());
    }
}
