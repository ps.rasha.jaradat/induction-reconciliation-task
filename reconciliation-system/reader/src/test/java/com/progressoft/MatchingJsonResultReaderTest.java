package com.progressoft;
import com.progressoft.entities.MatchingRecord;
import com.progressoft.reader.resultReader.MatchingJsonResultReader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.List;

public class MatchingJsonResultReaderTest {
    private String NOT_FOUND_FILE = "/notFound.json";
    private String MATCHING = getClass().getClassLoader().getResource("matching.json").getPath();
    private String IN_VALID_FORMAT =getClass().getClassLoader().getResource("invalidFormat.json").getPath();

    @Test
    public void givenNotFoundJsonFile_whenRead_thenIllegalArgumentException() {
        IllegalStateException exception = Assertions.assertThrows(IllegalStateException.class,
                () -> new MatchingJsonResultReader().readMatch(NOT_FOUND_FILE));
        Assertions.assertEquals("file path you are trying to read not found, try another one", exception.getMessage());
    }
    @Test
    public void givenValidJson_whenRead_thenAllRecordInMap() {
        List<MatchingRecord> records = new MatchingJsonResultReader().readMatch(MATCHING);
        Assertions.assertEquals(3, records.size());
    }
    @Test
    public void givenFileWithInvalidJsonFormat_WhenRead_thenThrowRuntimeException() {
        RuntimeException exception =
                Assertions.assertThrows(RuntimeException.class,
                        () -> new MatchingJsonResultReader().readMatch(IN_VALID_FORMAT));
        Assertions.assertEquals("Error while parsing json,something wrong with file format",exception.getMessage());
    }
}

