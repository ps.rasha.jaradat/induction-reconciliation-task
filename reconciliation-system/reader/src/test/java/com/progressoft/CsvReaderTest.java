package com.progressoft;

import com.progressoft.entities.CsvRecord;
import com.progressoft.reader.exceptions.AmountFormatException;
import com.progressoft.reader.exceptions.DateFormatException;
import com.progressoft.reader.exceptions.IntegrityException;
import com.progressoft.reader.inputReader.CsvReader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;


public class CsvReaderTest {
    private final Path EMPTY_FILE = Paths.get(getClass().getClassLoader().getResource("empty_file.csv").getPath());
    private final Path WRONG_HEADER_FILE = Paths.get(getClass().getClassLoader().getResource("file_with_wrong_header.csv").getPath());
    private final Path FILE_WITH_NO_RECORD = Paths.get(getClass().getClassLoader().getResource("file_with_no_records.csv").getPath());
    private final Path VALID_CSV_FILE = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").getPath());
    private final Path DUPLICATE_RECORDS_FILE = Paths.get(getClass().getClassLoader().getResource("duplicateRaw.csv").getPath());
    private final Path IN_VALID_AMOUNT = Paths.get(getClass().getClassLoader().getResource("invalid_amount.csv").getPath());
    private final Path IN_VALID_DATE = Paths.get(getClass().getClassLoader().getResource("invalid_date.csv").getPath());
    private final Path WRONG_DATE_FORMAT = Paths.get(getClass().getClassLoader().getResource("wrong_dateFormat.csv").getPath());
    private final Path NOT_FOUND_FILE = Paths.get("/notFound.csv");

    @Test
    public void givenEmptyCsvFile_whenRead_thenThrowIllegalArgumentException() {
        IllegalArgumentException exception =
                Assertions.assertThrows(IllegalArgumentException.class, () -> new CsvReader().read(EMPTY_FILE));
        Assertions.assertEquals("csv File is empty!", exception.getMessage());
    }

    @Test
    public void givenCsvFileWithWrongHeader_whenRead_thenThrowIllegalArgumentException() {
        IllegalArgumentException exception =
                Assertions.assertThrows(IllegalArgumentException.class, () -> new CsvReader().read(WRONG_HEADER_FILE));
        Assertions.assertEquals("csv File header is not correct it should be : " +
                "trans unique id,trans description,amount,currecny,purpose,value date,trans type", exception.getMessage());
    }

    @Test
    public void givenCsvFileWithNoRecord_whenRead_thenThrowIllegalArgumentException() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CsvReader().read(FILE_WITH_NO_RECORD));
        Assertions.assertEquals("csv File with no record!", exception.getMessage());
    }

    @Test
    public void givenCsvFileAndRecordMap_whenRead_thenAllRecordsInMap() {
        Map<String, CsvRecord> expectedRecordMap = getExpectedMap();
        Map<String, CsvRecord> actualRecordMap = new CsvReader().read(VALID_CSV_FILE);
        Assertions.assertEquals(6, actualRecordMap.size());
        for (String s : actualRecordMap.keySet()) {
            Assertions.assertEquals(expectedRecordMap.get(s).getTransactionId(), actualRecordMap.get(s).getTransactionId());
            Assertions.assertEquals(expectedRecordMap.get(s).getTransactionAmount(), actualRecordMap.get(s).getTransactionAmount());
            Assertions.assertEquals(expectedRecordMap.get(s).getTransactionDate(), actualRecordMap.get(s).getTransactionDate());
            Assertions.assertEquals(expectedRecordMap.get(s).getTransactionCurrencyCode(), actualRecordMap.get(s).getTransactionCurrencyCode());

        }
    }

    @Test
    public void givenWrongDateFormat_whenRead_thenThrowDateFormatException() {
        DateFormatException exception = Assertions.assertThrows(DateFormatException.class,
                () -> new CsvReader().read(WRONG_DATE_FORMAT));
        Assertions.assertEquals("Date '2020/02/31' format is not correct, It should be YYYY-MM-DD", exception.getMessage());
    }

    @Test
    public void givenRecordWithInValidDate_whenRead_thenThrowDateFormatException() {
        DateFormatException exception = Assertions.assertThrows(DateFormatException.class,
                () -> new CsvReader().read(IN_VALID_DATE));
        Assertions.assertEquals("Date '2020-50-31' is not correct!", exception.getMessage());
    }

    @Test
    public void givenInvalidAmount_whenRead_thenThrowAmountFormatException() {
        AmountFormatException exception = Assertions.assertThrows(AmountFormatException.class,
                () -> new CsvReader().read(IN_VALID_AMOUNT));
        Assertions.assertEquals("This amount 'abc' is not number", exception.getMessage());
    }

    @Test
    public void givenDuplicateRaw_whenRead_thenThrowIntegrityException() {
        IntegrityException exception = Assertions.assertThrows(IntegrityException.class,
                () -> new CsvReader().read(DUPLICATE_RECORDS_FILE));
        Assertions.assertEquals("It looks like there is a repeated rows,or there is a transaction id that used in more than one row", exception.getMessage());
    }

    @Test
    public void givenNotFoundFile_whenRead_thenThrowIntegrityException() {
        IllegalStateException exception = Assertions.assertThrows(IllegalStateException.class,
                () -> new CsvReader().read(NOT_FOUND_FILE));
        Assertions.assertEquals("Error occurred while opening csv file, file might not be found!", exception.getMessage());
    }

    private Map<String, CsvRecord> getExpectedMap() {
        Map<String, CsvRecord> map = new HashMap<>();
        map.put("TR-47884222201", new CsvRecord("TR-47884222201", new BigDecimal("140.00"), "USD", LocalDate.of(2020, 01, 20)));
        map.put("TR-47884222202", new CsvRecord("TR-47884222202", new BigDecimal("20.000"), "JOD", LocalDate.of(2020, 01, 22)));
        map.put("TR-47884222203", new CsvRecord("TR-47884222203", new BigDecimal("5000.000"), "JOD", LocalDate.of(2020, 01, 25)));
        map.put("TR-47884222204", new CsvRecord("TR-47884222204", new BigDecimal("1200.000"), "JOD", LocalDate.of(2020, 01, 31)));
        map.put("TR-47884222205", new CsvRecord("TR-47884222205", new BigDecimal("60.000"), "JOD", LocalDate.of(2020, 02, 02)));
        map.put("TR-47884222206", new CsvRecord("TR-47884222206", new BigDecimal("500.00"), "USD", LocalDate.of(2020, 02, 10)));
        return map;
    }
}


