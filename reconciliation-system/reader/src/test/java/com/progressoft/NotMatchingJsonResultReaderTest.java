package com.progressoft;

import com.progressoft.entities.NotMatchingRecord;
import com.progressoft.reader.resultReader.NotMatchingJsonResultReader;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.io.IOException;
import java.util.List;

public class NotMatchingJsonResultReaderTest {
    private final String MISS_MATCHING = getClass().getClassLoader().getResource("miss_matching.json").getPath();
    private final String MISSING = getClass().getClassLoader().getResource("missing.json").getPath();
    @Test
    public void givenMissingJson_whenRead_thenAllRecordInMap() {
        List<NotMatchingRecord> records = new NotMatchingJsonResultReader().readNotMatch(MISSING);
        Assertions.assertEquals(3, records.size());
    }
    @Test
    public void givenMissMatchingCsv_whenRead_thenAllRecordInMap() throws IOException {
        List<NotMatchingRecord> records = new NotMatchingJsonResultReader().readNotMatch(MISS_MATCHING);
        Assertions.assertEquals(4, records.size());
    }
}
