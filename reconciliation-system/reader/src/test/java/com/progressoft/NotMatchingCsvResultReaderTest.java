package com.progressoft;
import com.progressoft.entities.NotMatchingRecord;
import com.progressoft.reader.resultReader.NotMatchingCsvResultReader;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import java.io.IOException;
import java.util.List;
public class NotMatchingCsvResultReaderTest {
    private final String MISS_MATCHING = getClass().getClassLoader().getResource("miss_matching.csv").getPath();
    private final String MISSING = getClass().getClassLoader().getResource("missing.csv").getPath();
    @Test
    public void givenMissingCsv_whenRead_thenAllRecordInMap() throws IOException {
        List<NotMatchingRecord> records = new NotMatchingCsvResultReader().readNotMatch(MISSING);
        Assertions.assertEquals(3, records.size());
    }
    @Test
    public void givenMissMatchingCsv_whenRead_thenAllRecordInMap() throws IOException {
        List<NotMatchingRecord> records = new NotMatchingCsvResultReader().readNotMatch(MISS_MATCHING);
        Assertions.assertEquals(4, records.size());
    }
}
