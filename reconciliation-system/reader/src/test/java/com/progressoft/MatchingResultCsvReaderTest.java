package com.progressoft;
import com.progressoft.entities.MatchingRecord;
import com.progressoft.reader.resultReader.MatchingResultCsvReader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.List;

public class MatchingResultCsvReaderTest {
    private final String MATCHING = getClass().getClassLoader().getResource("matching.csv").getPath();
    private final String NOT_FOUND_FILE = "/notFound.csv";
    @Test
    public void givenNotFoundCsvFile_whenRead_thenIllegalArgumentException() {
        IllegalStateException exception = Assertions.assertThrows(IllegalStateException.class,
                () -> new MatchingResultCsvReader().readMatch(NOT_FOUND_FILE));
        Assertions.assertEquals("file path you are trying to read not found, try another one", exception.getMessage());
    }

    @Test
    public void givenMatchingCsv_whenRead_thenAllRecordInMap(){
        List<MatchingRecord> records = new MatchingResultCsvReader().readMatch(MATCHING);
        Assertions.assertEquals(3, records.size());
    }


}
