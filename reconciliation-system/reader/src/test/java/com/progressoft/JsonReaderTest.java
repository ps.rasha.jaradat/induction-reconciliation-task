package com.progressoft;

import com.progressoft.entities.JsonRecord;
import com.progressoft.reader.exceptions.AmountFormatException;
import com.progressoft.reader.exceptions.DateFormatException;
import com.progressoft.reader.exceptions.IntegrityException;
import com.progressoft.reader.inputReader.JsonReader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class JsonReaderTest {
    private Path EMPTY_FILE = Paths.get(getClass().getClassLoader().getResource("empty_file.json").getPath());
    private Path WRONG_HEADER = Paths.get(getClass().getClassLoader().getResource("wrong_header.json").getPath());
    private Path VALID_FILE =Paths.get(getClass().getClassLoader().getResource("online-banking-transactions.json").getPath());
    private Path WRONG_DATE_FORMAT = Paths.get(getClass().getClassLoader().getResource("fileWithWrongDateFormat.json").getPath());
    private Path IN_VALID_FORMAT =Paths.get(getClass().getClassLoader().getResource("invalidFormat.json").getPath());
    private Path IN_VALID_AMOUNT = Paths.get(getClass().getClassLoader().getResource("invalidAmount.json").getPath());
    private Path DUPLICATE_RAW = Paths.get(getClass().getClassLoader().getResource("file_with_duplicate_raw.json").getPath());
    private Path NOT_FOUND_FILE = Paths.get( "/notFound.json");
    @Test
    public void givenFileWithWrongDateFormat_WhenRead_thenThrowDateFormatException() {
        DateFormatException exception =
                Assertions.assertThrows(DateFormatException.class,
                        () -> new JsonReader().read(WRONG_DATE_FORMAT));
        Assertions.assertEquals("date : 20-01-2020 has wrong format, use DD/MM/YYYY", exception.getMessage());
    }
    @Test
    public void givenFileWithInvalidJsonFormat_WhenRead_thenThrowRuntimeException() {
        RuntimeException exception =
                Assertions.assertThrows(RuntimeException.class,
                        () -> new JsonReader().read(IN_VALID_FORMAT));
        Assertions.assertEquals("Error while parsing json,something wrong with file format",exception.getMessage());
    }
    @Test
    public void givenFileWithInvalidAmount_WhenRead_thenThrowAmountFormatException() {
        AmountFormatException exception =
                Assertions.assertThrows(AmountFormatException.class,
                        () -> new JsonReader().read(IN_VALID_AMOUNT));
        Assertions.assertEquals("Amount 'ABC' is not correct", exception.getMessage());
    }
    @Test
    public void givenJsonFileWithWrongHeader_WhenRead_thenThrowAmountFormatException() {
        IllegalArgumentException exception =
                Assertions.assertThrows(IllegalArgumentException.class,
                        () -> new JsonReader().read(WRONG_HEADER));
        Assertions.assertEquals("Record : 1 with inCorrect header, key : 'transUniqueId'", exception.getMessage());
    }
    @Test
    public void givenEmptyJsonFile_whenRead_thenThrowRuntimeException() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new JsonReader().read(EMPTY_FILE));
        Assertions.assertEquals("Json file if Empty", exception.getMessage());
    }
    @Test
    public void givenJsonFile_whenRead_thenIllegalArgumentException() {
        IllegalStateException exception = Assertions.assertThrows(IllegalStateException.class,
                () -> new JsonReader().read(NOT_FOUND_FILE));
        Assertions.assertEquals("file path you are trying to read not found, try another one", exception.getMessage());
    }
    @Test
    public void givenValidJson_whenRead_thenAllRecordInMap() {
        Map<String, JsonRecord> actualMap = new JsonReader().read(VALID_FILE);
        Assertions.assertEquals(7, actualMap.size());
        Map<String, JsonRecord> expectedMap = new HashMap<>();
        fillExpectedMap(expectedMap);
        for (String s : actualMap.keySet()) {
            Assertions.assertEquals(expectedMap.get(s).getTransactionId(), actualMap.get(s).getTransactionId());
            Assertions.assertEquals(expectedMap.get(s).getTransactionDate(), actualMap.get(s).getTransactionDate());
            Assertions.assertEquals(expectedMap.get(s).getTransactionAmount(), actualMap.get(s).getTransactionAmount());
            Assertions.assertEquals(expectedMap.get(s).getTransactionCurrencyCode(), actualMap.get(s).getTransactionCurrencyCode());
        }
    }
    @Test
    public void givenDuplicateRaw_whenRead_thenThrowIntegrityException() {
        IntegrityException exception = Assertions.assertThrows(IntegrityException.class,
                () -> new JsonReader().read(DUPLICATE_RAW));
        Assertions.assertEquals("It looks like there is a repeated rows,or there is a transaction id that used in more than one row", exception.getMessage());
    }
    private void fillExpectedMap(Map<String, JsonRecord> expectedMap) {
        expectedMap.put("TR-47884222201", new JsonRecord(LocalDate.of(2020, 01, 20), "TR-47884222201", new BigDecimal("140.00"), "USD", "donation"));
        expectedMap.put("TR-47884222205", new JsonRecord(LocalDate.of(2020, 02, 03), "TR-47884222205", new BigDecimal("60.000"), "JOD", ""));
        expectedMap.put("TR-47884222202", new JsonRecord(LocalDate.of(2020, 01, 22), "TR-47884222202", new BigDecimal("30.000"), "JOD", "donation"));
        expectedMap.put("TR-47884222217", new JsonRecord(LocalDate.of(2020, 02, 14), "TR-47884222217", new BigDecimal("12000.000"), "JOD", "salary"));
        expectedMap.put("TR-47884222203", new JsonRecord(LocalDate.of(2020, 01, 25), "TR-47884222203", new BigDecimal("5000.000"), "JOD", "not specified"));
        expectedMap.put("TR-47884222245", new JsonRecord(LocalDate.of(2020, 01, 12), "TR-47884222245", new BigDecimal("420.00"), "USD", "loan"));
        expectedMap.put("TR-47884222206", new JsonRecord(LocalDate.of(2020, 02, 10), "TR-47884222206", new BigDecimal("500.00"), "USD", "general"));

    }
}
