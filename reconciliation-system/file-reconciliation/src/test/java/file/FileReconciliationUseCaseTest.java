package file;

import com.progressoft.validator.RequestValidator;
import org.junit.jupiter.api.*;
import com.progressoft.fileSystemUseCase.FileReconciliationUseCase;
import com.progressoft.reconciliation.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileReconciliationUseCaseTest {

    @Test
    public void givenNullValidator_whenReconcile_thenThrowNullPointerException(){
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class ,
                ()->new FileReconciliationUseCase(null));
        Assertions.assertEquals("Validator is null!" , exception.getMessage());
    }
    @Test
    public void givenValidReconRequest_whenReconcile_thenReturnCorrectMissMatchingResult() throws IOException {
        FileReconciliationUseCase reconciliation = new FileReconciliationUseCase(new RequestValidator());
        List<String> result = reconciliation.process(MockValidReconRequest.VALID_RECON_REQUEST);
        try (BufferedReader br = Files.newBufferedReader(Paths.get(result.get(0)))) {
            String actualHeader = br.readLine();
            String actualLine1 = br.readLine();
            String actualLine2 = br.readLine();
            String actualLine3 = br.readLine();
            String expectedHeader = "transaction id,amount,currecny code,value date";
            String expectedLine1 = "TR-47884222206,500.00,USD,2020-02-10";
            String expectedLine2 = "TR-47884222203,5000.000,JOD,2020-01-25";
            String expectedLine3 = "TR-47884222201,140.00,USD,2020-01-20";
            Assertions.assertEquals(expectedHeader, actualHeader);
            Assertions.assertEquals(expectedLine1, actualLine1);
            Assertions.assertEquals(expectedLine2, actualLine2);
            Assertions.assertEquals(expectedLine3, actualLine3);
            Assertions.assertFalse(br.ready());
        }
        try (BufferedReader br = Files.newBufferedReader(Paths.get(result.get(1)))) {
            String actualHeader = br.readLine();
            String actualLine1 = br.readLine();
            String actualLine2 = br.readLine();
            String actualLine3 = br.readLine();
            String actualLine4 = br.readLine();

            String expectedHeader = "found in file,transaction id,amount,currecny code,value date";
            String expectedLine1 = "SOURCE,TR-47884222202,20.000,JOD,2020-01-22";
            String expectedLine2 = "TARGET,TR-47884222202,30.000,JOD,2020-01-22";
            String expectedLine3 = "SOURCE,TR-47884222205,60.000,JOD,2020-02-02";
            String expectedLine4 = "TARGET,TR-47884222205,60.000,JOD,2020-02-03";
            Assertions.assertEquals(expectedHeader, actualHeader);
            Assertions.assertEquals(expectedLine1, actualLine1);
            Assertions.assertEquals(expectedLine2, actualLine2);
            Assertions.assertEquals(expectedLine3, actualLine3);
            Assertions.assertEquals(expectedLine4, actualLine4);
            Assertions.assertFalse(br.ready());
        }
        try (BufferedReader br = Files.newBufferedReader(Paths.get(result.get(2)))) {
            String actualHeader = br.readLine();
            String actualLine1 = br.readLine();
            String actualLine2 = br.readLine();
            String actualLine3 = br.readLine();
            String expectedHeader = "found in file,transaction id,amount,currency code,value date";
            String expectedLine1 = "SOURCE,TR-47884222204,1200.000,JOD,2020-01-31";
            String expectedLine2 = "TARGET,TR-47884222217,12000.000,JOD,2020-02-14";
            String expectedLine3 = "TARGET,TR-47884222245,420.00,USD,2020-01-12";
            Assertions.assertEquals(expectedHeader, actualHeader);
            Assertions.assertEquals(expectedLine1, actualLine1);
            Assertions.assertEquals(expectedLine2, actualLine2);
            Assertions.assertEquals(expectedLine3, actualLine3);
            Assertions.assertFalse(br.ready());
        }
        deleteCreatedFiles(result);
    }

    private void deleteCreatedFiles(List<String> result) throws IOException {
        Files.delete(Paths.get(result.get(0)));
        Files.delete(Paths.get(result.get(1)));
        Files.delete(Paths.get(result.get(2)));
        Files.delete(Paths.get(System.getProperty("user.home") + "/recon_result"));
    }


}

