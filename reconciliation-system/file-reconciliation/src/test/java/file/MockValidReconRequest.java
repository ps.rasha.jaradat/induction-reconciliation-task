package file;
import com.progressoft.reconciliation.ReconRequest;

public interface MockValidReconRequest extends ReconRequest {
    ReconRequest VALID_RECON_REQUEST = new ReconRequest() {
        @Override
        public String getSourceFile() {
            return getClass().getClassLoader().getResource("bank-transactions.csv").getPath();
        }

        @Override
        public String getSourceType() {
            return "csv";
        }

        @Override
        public String getTargetFile() {
            return getClass().getClassLoader().getResource("online-banking-transactions.json").getPath();
        }

        @Override
        public String getTargetType() {
            return "json";
        }
    };
}
