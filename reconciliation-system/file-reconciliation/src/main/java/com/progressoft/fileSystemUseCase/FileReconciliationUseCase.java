package com.progressoft.fileSystemUseCase;

import com.progressoft.generator.GeneratorFactory;
import com.progressoft.reconcileUseCase.ReconUseCase;
import com.progressoft.reconciliation.ReconRequest;
import com.progressoft.reconciliation.ReconCompareResult;
import com.progressoft.reconciliation.ResponseGenerator;
import com.progressoft.reconciliation.TransactionRecord;
import com.progressoft.reader.inputReader.ReaderFactory;
import com.progressoft.reader.inputReader.FileReader;
import com.progressoft.validator.Validator;


import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class FileReconciliationUseCase extends ReconUseCase<ReconRequest, List<String>> {

    private Validator<ReconRequest> validator;

    public FileReconciliationUseCase(Validator<ReconRequest> validator) {
        if (Objects.isNull(validator))
            throw new NullPointerException("Validator is null!");
        this.validator = validator;
    }

    @Override
    protected void validateRequest(ReconRequest reconRequest) {
        validator.isValid(reconRequest);
    }

    @Override
    protected Map<String, ? extends TransactionRecord> readSource(ReconRequest request) {
        return readFile(request.getSourceType(), request.getSourceFile());
    }

    @Override
    protected Map<String, ? extends TransactionRecord> readTarget(ReconRequest request) {
        return readFile(request.getTargetType(), request.getTargetFile());
    }

    @Override
    protected List<String> generateResponse(ReconCompareResult result) {
        ResponseGenerator <String> resultGenerator =
                (ResponseGenerator<String>) GeneratorFactory.getGenerator("csv" ,System.getProperty("user.home"));
        return resultGenerator.generate(result);
    }

    private Map<String, ? extends TransactionRecord> readFile(String type, String filePath) {
        FileReader fileReader = ReaderFactory.getReader(type);
        return fileReader.read(Paths.get(filePath));
    }


}
