package com.progressoft.reconciliation;

import org.junit.jupiter.api.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReconComparatorTest {
    private Map<String, TransactionRecord> source;
    private Map<String, TransactionRecord> target;

    @BeforeEach
    public void beforeEach() {
        source = new HashMap<>();
        target = new HashMap<>();
        source.put("id1", new MockTransactionRecord("id1", new BigDecimal("10.00"), "USD", LocalDate.of(2020, 01, 01)));
        source.put("id2", new MockTransactionRecord("id2", new BigDecimal("20.00"), "JOD", LocalDate.of(2020, 01, 02)));
        source.put("id5", new MockTransactionRecord("id5", new BigDecimal("10.00"), "JOD", LocalDate.of(2020, 01, 02)));
        target.put("id1", new MockTransactionRecord("id1", new BigDecimal("10.00"), "USD", LocalDate.of(2020, 01, 01)));
        target.put("id3", new MockTransactionRecord("id3", new BigDecimal("10.00"), "USD", LocalDate.of(2020, 01, 01)));
        target.put("id2", new MockTransactionRecord("id2", new BigDecimal("10.00"), "USD", LocalDate.of(2020, 01, 02)));
        target.put("id4", new MockTransactionRecord("id4", new BigDecimal("10.00"), "USD", LocalDate.of(2020, 01, 01)));

    }
    @Test
    public void givenNullSourceMap_whenCompare_thenThrowNullPointerException(){
        source = null;
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class ,
                ()->new ReconComparator().compare(source ,target));
        Assertions.assertEquals("The Source you are trying to compare with is null!",exception.getMessage());
    }
    @Test
    public void givenNullTargetMap_whenCompare_thenThrowNullPointerException(){
        target = null;
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class ,
                ()->new ReconComparator().compare(source ,target));
        Assertions.assertEquals("The Target you are trying to compare with is null!",exception.getMessage());
    }
    @Test
    public void givenSourceAndTarget_whenCompare_thenMatchedTransactionAreInMatchingMap() {

        ReconComparator reconComparator = new ReconComparator();
        ReconCompareResult result = reconComparator.compare(source, target);
        List<TransactionRecord> actualResult = result.getMatching();
        Assertions.assertEquals(1, actualResult.size());
        List<TransactionRecord> expectedResult = new ArrayList<>();
        expectedResult.add(new MockTransactionRecord("id1", new BigDecimal("10.00"), "USD", LocalDate.of(2020, 01, 01)));
        for (int i = 0; i < 1; i++) {
            Assertions.assertEquals(expectedResult.get(i), actualResult.get(i));
        }
    }

    @Test
    public void givenSourceAndTarget_whenCompare_thenMissMatchTransactionAreInMissMatchingMap() {
        ReconComparator reconComparator = new ReconComparator();
        ReconCompareResult result = reconComparator.compare(source, target);
        List<TransactionTypePair> actualResult = result.getMissMatching();
        Assertions.assertEquals(2, actualResult.size());
        List<TransactionTypePair> expectedResult = getExpectedMissMatchList();
        for (int i = 0; i < 2; i++) {
            Assertions.assertEquals(expectedResult.get(i).getRecord(), actualResult.get(i).getRecord());
            Assertions.assertEquals(expectedResult.get(i).getType(), actualResult.get(i).getType());
        }
    }

    @Test
    public void givenSourceAndTarget_whenCompare_thenMissingTransactionAreInMissingMap() {
        ReconComparator reconComparator = new ReconComparator();
        ReconCompareResult result = reconComparator.compare(source, target);
        List<TransactionTypePair> actualResult = result.getMissing();
        Assertions.assertEquals(3, actualResult.size());
        List<TransactionTypePair> expectedResult = getExpectedMissingList();
        for (int i = 0; i < 3; i++) {
            Assertions.assertEquals(expectedResult.get(i).getRecord(), actualResult.get(i).getRecord());
            Assertions.assertEquals(expectedResult.get(i).getType(), actualResult.get(i).getType());
        }

    }

    private List<TransactionTypePair> getExpectedMissingList() {
        List<TransactionTypePair> list = new ArrayList<>();
        list.add(new TransactionTypePair("SOURCE", new MockTransactionRecord("id5", new BigDecimal("10.00"), "JOD", LocalDate.of(2020, 01, 02))));
        list.add(new TransactionTypePair("TARGET", new MockTransactionRecord("id4", new BigDecimal("10.00"), "USD", LocalDate.of(2020, 01, 01))));
        list.add(new TransactionTypePair("TARGET", new MockTransactionRecord("id3", new BigDecimal("10.00"), "USD", LocalDate.of(2020, 01, 01))));
        return list;
    }

    private List<TransactionTypePair> getExpectedMissMatchList() {
        List<TransactionTypePair> list = new ArrayList<>();
        list.add(new TransactionTypePair("SOURCE", new MockTransactionRecord("id2", new BigDecimal("20.00"), "JOD", LocalDate.of(2020, 01, 02))));
        list.add(new TransactionTypePair("TARGET", new MockTransactionRecord("id2", new BigDecimal("10.00"), "USD", LocalDate.of(2020, 01, 02))));
        return list;
    }
}


