package com.progressoft.reconciliation;

import com.progressoft.reconciliation.TransactionRecord;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public class MockTransactionRecord extends TransactionRecord {
    private String id;
    private BigDecimal amount;
    private LocalDate date;
    private String currencyCode;
    public MockTransactionRecord(String id , BigDecimal amount  , String currencyCode,LocalDate date){
        this.amount=amount;
        this.id=id;
        this.currencyCode=currencyCode;
        this.date=date;
    }

    @Override
    public String getTransactionId() {
        return id;
    }

    @Override
    public BigDecimal getTransactionAmount() {
        return amount;
    }

    @Override
    public LocalDate getTransactionDate() {
        return date;
    }

    @Override
    public String getTransactionCurrencyCode() {
        return currencyCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MockTransactionRecord that = (MockTransactionRecord) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(date, that.date) &&
                Objects.equals(currencyCode, that.currencyCode);
    }

}
