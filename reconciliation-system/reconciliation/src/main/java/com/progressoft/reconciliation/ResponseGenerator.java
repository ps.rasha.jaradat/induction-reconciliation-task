package com.progressoft.reconciliation;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class ResponseGenerator<RECON_RESULT> {
    private String path;

    public ResponseGenerator(String path) {
        this.path = path;
    }

    public final List<RECON_RESULT> generate(ReconCompareResult result) {
        validateReconResult(result);
        writeResultOnFiles(result, path);
        List<RECON_RESULT> resultList = new ArrayList<RECON_RESULT>();
        resultList.add(getMatchingTransactions());
        resultList.add(getMismatchingTransactions());
        resultList.add(getMissingTransactions());
        return resultList;
    }

    protected abstract void writeResultOnFiles(ReconCompareResult result, String path);

    protected abstract RECON_RESULT getMissingTransactions();

    protected abstract RECON_RESULT getMismatchingTransactions();

    protected abstract RECON_RESULT getMatchingTransactions();

    private void validateReconResult(ReconCompareResult result) {
        if (Objects.isNull(result))
            throw new NullPointerException("Given Null result !");
        if (Objects.isNull(result.getMatching()))
            throw new NullPointerException("Something went wrong, matching transaction are null!");
        if (Objects.isNull(result.getMissing()))
            throw new NullPointerException("Something went wrong, missing transaction are null!");
        if (Objects.isNull(result.getMissMatching()))
            throw new NullPointerException("Something went wrong, missMatching transaction are null!");

    }
}



