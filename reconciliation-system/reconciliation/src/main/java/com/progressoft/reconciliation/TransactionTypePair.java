package com.progressoft.reconciliation;
public class TransactionTypePair {
    private TransactionRecord record;
    private String type;

    public TransactionTypePair(String type, TransactionRecord record) {
        this.type = type;
        this.record = record;
    }

    public TransactionRecord getRecord() {
        return record;
    }
    public String getType() {
        return type;
    }
}
