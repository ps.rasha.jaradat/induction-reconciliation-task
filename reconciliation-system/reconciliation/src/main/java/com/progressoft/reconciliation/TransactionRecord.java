package com.progressoft.reconciliation;

import java.math.BigDecimal;
import java.time.LocalDate;

public abstract class TransactionRecord {
    public abstract String getTransactionId();

    public abstract BigDecimal getTransactionAmount();

    public abstract LocalDate getTransactionDate();

    public abstract String getTransactionCurrencyCode();
 }
