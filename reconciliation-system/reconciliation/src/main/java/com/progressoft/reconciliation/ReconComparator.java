package com.progressoft.reconciliation;

import java.util.*;

public class ReconComparator {
    private List<TransactionRecord> matching = new ArrayList<>();
    private List<TransactionTypePair> missMatching = new ArrayList<>();
    private List<TransactionTypePair> missing = new ArrayList<>();

    public ReconCompareResult compare(Map<String, ? extends TransactionRecord> source, Map<String, ? extends TransactionRecord> target) {
        validateMaps(source, target);
        return compareTransactions(source, target);
    }

    protected ReconCompareResult compareTransactions(Map<String, ? extends TransactionRecord> source,Map<String, ? extends TransactionRecord> target) {
        for (String key : source.keySet()) {
            TransactionRecord sourceRecord = source.get(key);
            TransactionRecord targetRecord = target.get(key);
            if (notInTarget(targetRecord)) {
                missing.add(new TransactionTypePair("SOURCE", sourceRecord));
                continue;
            }
            target.remove(key);
            if (isTranEqual(sourceRecord, targetRecord)) {
                matching.add(sourceRecord);
                continue;
            }
            missMatching.add(new TransactionTypePair("SOURCE", sourceRecord));
            missMatching.add(new TransactionTypePair("TARGET", targetRecord));
        }
        for (String key : target.keySet()) {
            missing.add(new TransactionTypePair("TARGET", target.get(key)));
        }
        return new ReconCompareResult(matching, missing, missMatching);
    }

    private boolean notInTarget(TransactionRecord targetRecord) {
        return Objects.isNull(targetRecord);
    }

    private void validateMaps(Map<String, ? extends TransactionRecord> source, Map<String, ? extends TransactionRecord> target) {
        if (Objects.isNull(source))
            throw new NullPointerException("The Source you are trying to compare with is null!");
        if (Objects.isNull(target))
            throw new NullPointerException("The Target you are trying to compare with is null!");
    }

    private boolean isTranEqual(TransactionRecord sourceRecord, TransactionRecord targetRecord) {
        return isAmountEqual(sourceRecord, targetRecord)
                && isCurrencyCodeEqual(sourceRecord, targetRecord)
                && isDateEquals(sourceRecord, targetRecord);
    }

    private boolean isDateEquals(TransactionRecord sourceRecord, TransactionRecord targetRecord) {
        return sourceRecord.getTransactionDate().equals(targetRecord.getTransactionDate());
    }

    private boolean isCurrencyCodeEqual(TransactionRecord sourceRecord, TransactionRecord targetRecord) {
        return sourceRecord.getTransactionCurrencyCode().equals(targetRecord.getTransactionCurrencyCode());
    }

    private boolean isAmountEqual(TransactionRecord sourceRecord, TransactionRecord targetRecord) {
        return sourceRecord.getTransactionAmount().compareTo(targetRecord.getTransactionAmount()) == 0;
    }
}
