package com.progressoft.reconciliation;
public interface ReconRequest {
    String getSourceFile();
    String getSourceType();
    String getTargetFile();
    String getTargetType();
}
