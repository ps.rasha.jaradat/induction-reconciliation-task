package com.progressoft.reconciliation;

import java.util.Collections;
import java.util.List;

public class ReconCompareResult {
    private List<TransactionRecord> matching;
    private List<TransactionTypePair> missing;
    private List<TransactionTypePair> missMatching;

    public ReconCompareResult(List<TransactionRecord> matching, List<TransactionTypePair> missing, List<TransactionTypePair> missMatching) {
        this.matching = matching;
        this.missing = missing;
        this.missMatching = missMatching;
    }

    public List<TransactionRecord> getMatching() {
        return Collections.unmodifiableList(matching);
    }

    public List<TransactionTypePair> getMissing() {
        return Collections.unmodifiableList(missing);
    }

    public List<TransactionTypePair> getMissMatching() {
        return Collections.unmodifiableList(missMatching);
    }
}
