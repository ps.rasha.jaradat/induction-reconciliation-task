package com.progressoft.reconcileUseCase;

import com.progressoft.reconciliation.ReconComparator;
import com.progressoft.reconciliation.ReconCompareResult;
import com.progressoft.reconciliation.TransactionRecord;

import java.util.Map;

public abstract class ReconUseCase<REQ, RES> {
    public final RES process(REQ request) {
        validateRequest(request);
        Map<String, ? extends TransactionRecord> source = readSource(request);
        Map<String, ? extends TransactionRecord> target = readTarget(request);
        ReconCompareResult result = compare(source, target);
        return generateResponse(result);
    }

    protected abstract void validateRequest(REQ request);

    protected abstract Map<String, ? extends TransactionRecord> readSource(REQ request);

    protected abstract Map<String, ? extends TransactionRecord> readTarget(REQ request);

    protected ReconCompareResult compare(Map<String, ? extends TransactionRecord> source, Map<String, ? extends TransactionRecord> target) {
        return new ReconComparator().compare(source, target);
    }

    protected abstract RES generateResponse(ReconCompareResult result);
}
