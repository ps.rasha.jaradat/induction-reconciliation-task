package com.progressoft.validator;

public interface Validator <T> {
    void isValid(T toValidate);
}
