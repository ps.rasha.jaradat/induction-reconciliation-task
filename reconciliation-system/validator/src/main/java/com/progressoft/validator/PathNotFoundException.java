package com.progressoft.validator;


public class PathNotFoundException extends RuntimeException{

    public PathNotFoundException(String message) {
        super(message);
    }

}
