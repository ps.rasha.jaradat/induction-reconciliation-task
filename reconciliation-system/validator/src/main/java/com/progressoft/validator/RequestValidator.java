package com.progressoft.validator;

import com.progressoft.reconciliation.ReconRequest;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

public class RequestValidator implements Validator<ReconRequest> {
    @Override
    public void isValid(ReconRequest request) {
        throwIfPathNotValid(request);
        throwIfExtensionNotValid(request);
    }

    private void throwIfPathNotValid(ReconRequest request) {
        validatePath(request.getSourceFile(), "Source file path");
        validatePath(request.getTargetFile(), "Target file path");
    }

    private void throwIfExtensionNotValid(ReconRequest request) {
        validateExtension(request.getSourceType(), "Source");
        validateExtension(request.getTargetType(), "Target");
        throwIfInconsistent(request.getSourceFile(), request.getSourceType(), "source");
        throwIfInconsistent(request.getTargetFile(), request.getTargetType(), "target");
    }

    private void validatePath(String path, String type) {
        throwIfNull(path, type);
        String filePath = path.trim();
        throwIfNotExist(type, filePath);
        throwIfDirectory(type, filePath);
    }

    private void validateExtension(String path, String type) {
        throwIfNull(path, type + " file type");
        String extension = path.trim().toLowerCase();
        throwIfExtensionNotAllowed(extension, type);
    }

    private void throwIfExtensionNotAllowed(String fileExtension, String type) {
        if (!isAllowedExtension(fileExtension))
            throw new IllegalArgumentException(type + " file type is not allowed, try another file type");
    }

    private void throwIfNull(String fileExtension, String type) {
        if (isNull(fileExtension))
            throw new NullPointerException(type + " is null!");
    }

    private boolean isAllowedExtension(String fileExtension) {
        ValidExtensions[] extensions = ValidExtensions.values();
        boolean isFound = false;
        for (ValidExtensions e : extensions) {
            if (e.name().toLowerCase().equals(fileExtension)) {
                isFound = true;
                break;
            }
        }
        return isFound;
    }

    private void throwIfDirectory(String type, String filePath) {
        if (Files.isDirectory(Paths.get(filePath)))
            throw new IllegalArgumentException(type + " is a directory path!");
    }

    private void throwIfNotExist(String type, String filePath) {
        if (isNotExist(filePath))
            throw new PathNotFoundException(type + " is not exist!");
    }

    private boolean isNotExist(String path) {
        return Files.notExists(Paths.get(path));
    }

    private boolean isNull(String s) {
        return Objects.isNull(s);
    }

    private void throwIfInconsistent(String filePath, String fileType, String type) {
        if (!filePath.endsWith("." + fileType.toLowerCase()))
            throw new IllegalArgumentException("Inconsistency between actual " + type + " type, and given " + type + " type!");
    }
}
