package com.progressoft.validator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.progressoft.reconciliation.ReconRequest;


public class RequestValidatorTest {
    private Validator<ReconRequest> validator;

    @BeforeEach
    public void BeforeEach() {
        validator = new RequestValidator();
    }

    @Test
    public void givenNullPaths_whenValidate_thenThrowNullPointerException() {
        NullPointerException exception =
                Assertions.assertThrows(NullPointerException.class, () -> validator.isValid(MockReconRequest.REQUEST_WITH_NULL_SOURCE_PATH));
        Assertions.assertEquals("Source file path is null!", exception.getMessage());
        exception = Assertions.assertThrows(NullPointerException.class,
                () -> validator.isValid(MockReconRequest.REQUEST_WITH_NULL_TARGET_PATH));
        Assertions.assertEquals("Target file path is null!", exception.getMessage());
    }

    @Test
    public void givenRequestWithNotExistPaths_whenValidate_thenThrowIllegalArgumentException() {
        PathNotFoundException exception =
                Assertions.assertThrows(PathNotFoundException.class, () -> validator.isValid(MockReconRequest.REQUEST_WITH_NOT_EXIST_SOURCE_PATH));
        Assertions.assertEquals("Source file path is not exist!", exception.getMessage());
        exception =
                Assertions.assertThrows(PathNotFoundException.class, () -> validator.isValid(MockReconRequest.REQUEST_WITH_NOT_EXIST_TARGET_PATH));
        Assertions.assertEquals("Target file path is not exist!", exception.getMessage());
    }

    @Test
    public void givenDirectory_whenValidate_thenThrowIllegalArgumentException() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> validator.isValid(MockReconRequest.REQUEST_WITH_DIRECTORY_SOURCE_PATH));
        Assertions.assertEquals("Source file path is a directory path!", exception.getMessage());
        exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> validator.isValid(MockReconRequest.REQUEST_WITH_DIRECTORY_TARGET_PATH));
        Assertions.assertEquals("Target file path is a directory path!", exception.getMessage());
    }

    @Test
    public void givenNullFileType_whenValidate_thenThrowNullPointerException() {
        NullPointerException exception =
                Assertions.assertThrows(NullPointerException.class,
                        () -> validator.isValid(MockReconRequest.REQUEST_WITH_NULL_SOURCE_FILE_TYPE));
        Assertions.assertEquals("Source file type is null!", exception.getMessage());
        exception = Assertions.assertThrows(NullPointerException.class,
                () -> validator.isValid(MockReconRequest.REQUEST_WITH_NULL_TARGET_FILE_TYPE));
        Assertions.assertEquals("Target file type is null!", exception.getMessage());
    }

    @Test
    public void givenNotValidFileType_whenValidate_thenThrowIllegalArgumentException() {
        IllegalArgumentException exception =
                Assertions.assertThrows(IllegalArgumentException.class, () -> validator.isValid(MockReconRequest.REQUEST_WITH_IN_VALID_SOURCE_FILE_TYPE));
        Assertions.assertEquals("Source file type is not allowed, try another file type", exception.getMessage());
        exception =
                Assertions.assertThrows(IllegalArgumentException.class, () -> validator.isValid(MockReconRequest.REQUEST_WITH_IN_VALID_TARGET_FILE_TYPE));
        Assertions.assertEquals("Target file type is not allowed, try another file type", exception.getMessage());
    }

    @Test
    public void givenValidFileSystemReconRequest_whenValidate_thenReturnTrue() {
        Assertions.assertDoesNotThrow(()->validator.isValid(MockReconRequest.VALID_RECON_REQUEST));
    }

    @Test
    public void givenInconsistentFilePathAndType_whenReconcile_theThrowIllegalArgumentException() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> validator.isValid(MockReconRequest.IN_CONSISTENT_SOURCE_PATH_AND_TYPE));
        Assertions.assertEquals("Inconsistency between actual source type, and given source type!", exception.getMessage());
        exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> validator.isValid(MockReconRequest.IN_CONSISTENT_TARGET_PATH_AND_TYPE));
        Assertions.assertEquals("Inconsistency between actual target type, and given target type!", exception.getMessage());
    }

}
