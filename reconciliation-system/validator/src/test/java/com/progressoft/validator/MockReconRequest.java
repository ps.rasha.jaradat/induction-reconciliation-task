package com.progressoft.validator;
import com.progressoft.reconciliation.ReconRequest;
public interface MockReconRequest extends ReconRequest {
    ReconRequest REQUEST_WITH_NULL_TARGET_PATH = new ReconRequest() {
        @Override
        public String getSourceFile() {
            return getClass().getClassLoader().getResource("bank-transactions.csv").getPath();
        }

        @Override
        public String getSourceType() {
            return "csv";
        }

        @Override
        public String getTargetFile() {
            return null;
        }

        @Override
        public String getTargetType() {
            return "json";
        }
    };
    ReconRequest REQUEST_WITH_NULL_SOURCE_PATH = new ReconRequest() {
        @Override
        public String getSourceFile() {
            return null;
        }

        @Override
        public String getSourceType() {
            return "csv";
        }

        @Override
        public String getTargetFile() {
            return getClass().getClassLoader().getResource("bank-transactions.csv").getPath();
        }

        @Override
        public String getTargetType() {
            return "json";
        }
    };
    ReconRequest REQUEST_WITH_NOT_EXIST_SOURCE_PATH = new ReconRequest() {
        @Override
        public String getSourceFile() {
            return "notExistFile.csv";
        }

        @Override
        public String getSourceType() {
            return "csv";
        }

        @Override
        public String getTargetFile() {
            return getClass().getClassLoader().getResource("bank-transactions.csv").getPath();
        }

        @Override
        public String getTargetType() {
            return "json";
        }
    };
    ReconRequest REQUEST_WITH_NOT_EXIST_TARGET_PATH = new ReconRequest() {
        @Override
        public String getSourceFile() {
            return getClass().getClassLoader().getResource("bank-transactions.csv").getPath();
        }

        @Override
        public String getSourceType() {
            return "csv";
        }

        @Override
        public String getTargetFile() {
            return "notExistFile.csv";
        }

        @Override
        public String getTargetType() {
            return "json";
        }
    };
    ReconRequest REQUEST_WITH_NULL_SOURCE_FILE_TYPE = new ReconRequest() {
        @Override
        public String getSourceFile() {
            return getClass().getClassLoader().getResource("bank-transactions.csv").getPath();
        }

        @Override
        public String getSourceType() {
            return null;
        }

        @Override
        public String getTargetFile() {
            return getClass().getClassLoader().getResource("bank-transactions.csv").getPath();
        }

        @Override
        public String getTargetType() {
            return "csv";
        }
    };
    ReconRequest REQUEST_WITH_NULL_TARGET_FILE_TYPE = new ReconRequest() {
        @Override
        public String getSourceFile() {
            return getClass().getClassLoader().getResource("bank-transactions.csv").getPath();
        }

        @Override
        public String getSourceType() {
            return "json";
        }

        @Override
        public String getTargetFile() {
            return getClass().getClassLoader().getResource("bank-transactions.csv").getPath();
        }

        @Override
        public String getTargetType() {
            return null;
        }
    };
    ReconRequest REQUEST_WITH_IN_VALID_SOURCE_FILE_TYPE = new ReconRequest() {
        @Override
        public String getSourceFile() {
            return getClass().getClassLoader().getResource("bank-transactions.csv").getPath();

        }

        @Override
        public String getSourceType() {
            return "not valid";
        }

        @Override
        public String getTargetFile() {
            return getClass().getClassLoader().getResource("bank-transactions.csv").getPath();

        }

        @Override
        public String getTargetType() {
            return "csv";
        }
    };
    ReconRequest REQUEST_WITH_IN_VALID_TARGET_FILE_TYPE = new ReconRequest() {
        @Override
        public String getSourceFile() {
            return getClass().getClassLoader().getResource("bank-transactions.csv").getPath();

        }

        @Override
        public String getSourceType() {
            return "json";
        }

        @Override
        public String getTargetFile() {
            return getClass().getClassLoader().getResource("bank-transactions.csv").getPath();

        }

        @Override
        public String getTargetType() {
            return "notValid";
        }
    };
    ReconRequest REQUEST_WITH_DIRECTORY_TARGET_PATH = new ReconRequest() {
        @Override
        public String getSourceFile() {
            return getClass().getClassLoader().getResource("bank-transactions.csv").getPath();
        }

        @Override
        public String getSourceType() {
            return "csv";
        }

        @Override
        public String getTargetFile() {
            return "./";
        }

        @Override
        public String getTargetType() {
            return "json";
        }
    };
    ReconRequest REQUEST_WITH_DIRECTORY_SOURCE_PATH = new ReconRequest() {
        @Override
        public String getSourceFile() {
            return "./";
        }

        @Override
        public String getSourceType() {
            return "csv";
        }

        @Override
        public String getTargetFile() {
            return getClass().getClassLoader().getResource("bank-transactions.csv").getPath();
        }

        @Override
        public String getTargetType() {
            return "json";
        }
    };
    ReconRequest VALID_RECON_REQUEST = new ReconRequest() {
        @Override
        public String getSourceFile() {
            return getClass().getClassLoader().getResource("bank-transactions.csv").getPath();
        }

        @Override
        public String getSourceType() {
            return "csv";
        }

        @Override
        public String getTargetFile() {
            return getClass().getClassLoader().getResource("online-banking-transactions.json").getPath();
        }

        @Override
        public String getTargetType() {
            return "json";
        }
    };
    ReconRequest IN_CONSISTENT_TARGET_PATH_AND_TYPE = new ReconRequest() {
        @Override
        public String getSourceFile() {
            return getClass().getClassLoader().getResource("online-banking-transactions.json").getPath();
        }

        @Override
        public String getSourceType() {
            return "json";
        }

        @Override
        public String getTargetFile() {
            return getClass().getClassLoader().getResource("online-banking-transactions.json").getPath();
        }

        @Override
        public String getTargetType() {
            return "csv";
        }
    };
            ReconRequest IN_CONSISTENT_SOURCE_PATH_AND_TYPE = new ReconRequest() {
                @Override
                public String getSourceFile() {
                    return getClass().getClassLoader().getResource("online-banking-transactions.json").getPath();
                }

                @Override
                public String getSourceType() {
                    return "csv";
                }

                @Override
                public String getTargetFile() {
                    return getClass().getClassLoader().getResource("online-banking-transactions.json").getPath();
                }

                @Override
                public String getTargetType() {
                    return "json";
                }
            };

}
